-- Admin user
insert into `User` (`id`, `enabled`, `password`, `username`) values('0','1','$2a$10$d72PvzK34SMt2SCSLp02l.KoM8IynYcudYuBAnXUWipjwEg24VdPS','admin');
insert into `Authority` (`id`, `authority`, `username`) values('0','ROLE_ADMIN','admin');

-- Categories and Products
insert into `Category` (`id`, `description`, `name`) values('0','Faux Plants Description','Faux Plants');
insert into `Category` (`id`, `description`, `name`) values('1','Flowers','Flowers');
insert into `Category` (`id`, `description`, `name`) values('2','Decorate','Decorate');

insert into `Product` (`id`, `description`, `discontinued`, `imageUrl`, `inCondition`, `manufacturer`, `name`, `productId`, `unitPrice`, `unitsInOrder`, `unitsInStock`, `category_id`) values('1','The shuffler plant originated in Taiwan and south China, and with our artificial tree, travelling isn\'t necessary to see one. Bent, thick trunk with elongated leaves at the top gives this faux tree is nickname; umbrella tree. 75-inches high, this tree is ideal for large, open spaces, and building or hotel lobbies. Like all our products, this one too is made with love by Bloomsbury Market.','','resources/upLoadImages/76%2BArtificial%2BEvergreen%2BTree.jpg','good','man','name pro2','P-12345','1.00','1','3','0');
insert into `Product` (`id`, `description`, `discontinued`, `imageUrl`, `inCondition`, `manufacturer`, `name`, `productId`, `unitPrice`, `unitsInOrder`, `unitsInStock`, `category_id`) values('2','','','resources/upLoadImages/35.4%2BArtificial%2BDracaena%2BPlant%2Bin%2BPot.jpg','good','Bay Isle Home','35.4\" Artificial Dracaena Plant in Pot','P-12346','59.99','1','300','0');
insert into `Product` (`id`, `description`, `discontinued`, `imageUrl`, `inCondition`, `manufacturer`, `name`, `productId`, `unitPrice`, `unitsInOrder`, `unitsInStock`, `category_id`) values('3','des7','','resources/upLoadImages/Live%20Fern%20Plant%20in%20Pot%20Set.jpg','good','Orren Ellis','Artificial Moss Topiary in Planter 1','P-12347','609.99','0','400','1');

UPDATE `hibernate_sequence` SET `next_val` = '4'; 