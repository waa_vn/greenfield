INSERT INTO User(id,username,password,enabled) VALUES (1,'admin','$2a$10$S/wlXEo/APzf.Sn1cO2p4.V12EJmaw.uzrHelMvkpuahjmHWnSafe', TRUE);
INSERT INTO User(id,username,password,enabled) VALUES (2,'guest','$2a$10$0.ESlGysrPaiW5HaapKwoehzWt5AibgbPPOvMhDv8D6H26QQ/CwhS', TRUE);
  
INSERT INTO Authority (id,username, authority) VALUES (1,'guest', 'ROLE_USER');
INSERT INTO Authority (id,username, authority) VALUES (2,'admin', 'ROLE_ADMIN');
INSERT INTO Authority (id,username, authority) VALUES (3,'admin', 'ROLE_USER');

INSERT INTO Category(`id`, `description`, `name`) VALUES ('1', 'Faux Plants Description', 'Faux Plants'); 
INSERT INTO Product(`id`,`productId`, `category_id`, `description`, `name`, `unitPrice`, `discontinued`, `unitsInStock`, `unitsInOrder`) VALUES (1, 'P-12345', 1, 'des', 'name pro', '1', '0', '1', '200');
