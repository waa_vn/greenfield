(function ($, utils, contextPath) {
  'use strict';

  const chatEndpoint = contextPath + '/chat_endpoint';

  function createUUID() {
    let dt = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  function Seller() {
    this.username = 'seller';
    this.stompClient = null;
    this.container = $('#seller');
    this.customers = {};
    this.selectedCustomer = null;
  }
  Seller.prototype = {
    init() {
      this.container.find('.connecting').hide();
      this.container.find('.btn-send')
        .prop('disabled', true)
        .on('click', this.send.bind(this));
      this.container.find('.inputMessage').on('keypress', this._onKeyPress.bind(this));
      this.connect();
    },

    connect() {
      if (!this.stompClient) {
        this.container.find('.connecting').show();
        const socket = new SockJS(chatEndpoint);
        this.stompClient = Stomp.over(socket)
        this.stompClient.connect({}, this._onConnected.bind(this), this._onError.bind(this));
      }
    },

    send() {
      if (this.selectedCustomer) {
        const messageInput = this.container.find('.inputMessage');
        const messageContent = messageInput.val().trim();
        if (messageContent) {
          const chatMessage = {
            sender: this.username,
            to: this.selectedCustomer.name,
            content: messageInput.val(),
            type: 'CHAT'
          };
          this.stompClient.send("/chat/seller.reply", {}, JSON.stringify(chatMessage));
          messageInput.val('');
        }
      }
    },

    _onKeyPress(event) {
      if (event.keyCode === 13) { //Enter
        this.send();
      }
    },
    _onConnected(frame) {
      this.username = frame.headers['user-name'];

      this.stompClient.subscribe('/secured/questions', this._onMessageReceived.bind(this));

      this.stompClient.send("/chat/seller.join", {}, JSON.stringify({ sender: this.username, type: 'JOIN' }));

      this.container.find('.btn-send')
        .prop('disabled', false);
      this.container.find('.connecting').hide();
    },
    _onError(error) {
      const connectingElement = this.container.find('.connecting');
      connectingElement.text('Could not connect to WebSocket server. Please refresh this page to try again!');
      connectingElement.css('color', 'red');
    },
    _onMessageReceived(payload) {
      const message = JSON.parse(payload.body);
      if (message) {
        const sender = message.sender;
        const type = message.type;
        let customer = this.customers[sender];
        if (type === 'LEAVE') {
          if (customer) {
            delete this.customers[sender];
            if (customer === this.selectedCustomer) {
              this.selectedCustomer = null;
              this._renderMessageOfSelectedCustomer();
            }
            this._removeExistingCustomer(customer);
          }
        } else {
          const isNew = !customer;
          if (isNew) {
            this.customers[sender] = customer = {
              name: sender,
              id: createUUID(),
              messages: []
            };
            this._renderNewCustomer(customer);
          }

          customer.messages.push(message);
          if (this.selectedCustomer === customer) {
            this._renderMessageOfSelectedCustomer();
          }
        }
      }
    },
    _getAvatarColor(messageSender) {
      let hash = 0;
      for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
      }
      const index = Math.abs(hash % utils.colors.length);
      return utils.colors[index];
    },
    _renderNewCustomer(newCustomer) {
      const customerList = this.container.find('.customer-list');
      const customerElement = $('<li id="' + newCustomer.id + '"><p>' + newCustomer.name + '</p></li>');

      customerList.append(customerElement);
      customerElement.on('click', this._onSelectedCustomer.bind(this, newCustomer));
    },
    _removeExistingCustomer(customer) {
      const customerElement = this.container.find('.customer-list #'+customer.id);
      customerElement.remove();
    },
    _renderMessageOfSelectedCustomer() {
      const messageArea = this.container.find('.messageArea');
      messageArea.html('');
      if (this.selectedCustomer) {
        const messages = this.selectedCustomer.messages;
        messages.forEach(message => {
          const messageElement = document.createElement('li');
          if (message.type === 'JOIN') {
            messageElement.classList.add('event-message');
            message.content = message.sender + ' joined!';
          } else if (message.type === 'LEAVE') {
            messageElement.classList.add('event-message');
            message.content = message.sender + ' left!';
          } else {
            messageElement.classList.add('chat-message');

            const avatarElement = document.createElement('i');
            const avatarText = document.createTextNode(message.sender[0]);
            avatarElement.appendChild(avatarText);
            avatarElement.style['background-color'] = this._getAvatarColor(message.sender);

            messageElement.appendChild(avatarElement);

            const usernameElement = document.createElement('span');
            const usernameText = document.createTextNode(message.sender);
            usernameElement.appendChild(usernameText);
            messageElement.appendChild(usernameElement);
          }

          const textElement = document.createElement('p');
          const messageText = document.createTextNode(message.content);
          textElement.appendChild(messageText);

          messageElement.appendChild(textElement);

          messageArea[0].appendChild(messageElement);
          messageArea[0].scrollTop = messageArea[0].scrollHeight;
        });
      }
    },
    _onSelectedCustomer(customer, event) {
      this.selectedCustomer = customer;
      this._renderMessageOfSelectedCustomer();
      this.container.find('.customer-list li').css('background-color', 'none').css('color', 'inherit');
      $(event.target).css('background-color', '#128ff2').css('color', 'white');
    }
  }

  $(document).ready(() => {
    new Seller().init();
  });
})(jQuery, window.UTILS, window.CONTEXT_PATH);