(function ($, contextPath) {
  function HomePage() {
    this.categroyMenu = $('#categoryMenuId');
  }
  HomePage.prototype = {
    loadAndRenderCategoryMenu() {
      this.loadCategories()
        .then(categories => this.renderCategoryMenu(categories))
        .catch();
    },
    loadCategories() {
      return new Promise((resolve, reject) =>
        $.ajax({
          type: 'GET',
          url: contextPath + '/api/categories',
          dataType: 'json',
          success: (data) => resolve(data),
          error: (errorObject) => reject(errorObject)
        })
      );
    },
    renderCategoryMenu(categories) {
      if (categories && categories.length) {
        for (let index = 0; index < categories.length; index++) {
          const category = categories[index];
          this.categroyMenu.append($('<li class="list-group-item"><a href="' + contextPath + '/category/' + category.id + '">' + category.name + '</a></li>'));
        }
      } else {
        this.categroyMenu.hide();
      }
    }
  }
  $(document).ready(function () {
    const page = new HomePage();
    page.loadAndRenderCategoryMenu();
  });
})(jQuery, window.CONTEXT_PATH);