(function ($, utils, contextPath) {
  'use strict';

  const chatEndpoint = contextPath + '/chat_endpoint';

  function Customer() {
    this.username = 'customer';
    this.stompClient = null;
    this.container = $('#customer');
  }
  Customer.prototype = {
    init() {
      this.container.find('.connecting').hide();
      this.container.find('.btn-send')
        .prop('disabled', true)
        .on('click', this.send.bind(this));
      this.container.find('.inputMessage').on('keypress', this._onKeyPress.bind(this));
      this.connect();
    },

    connect() {
      this.container.find('.connecting').show();
      if (!this.stompClient) {
        const socket = new SockJS(chatEndpoint);
        this.stompClient = Stomp.over(socket)
        this.stompClient.connect({}, this._onConnected.bind(this), this._onError.bind(this));
      }
    },

    send() {
      const messageInput = this.container.find('.inputMessage');
      const messageContent = messageInput.val().trim();
      if (messageContent) {
        const chatMessage = {
          sender: this.username,
          content: messageInput.val(),
          type: 'CHAT'
        };
        this.stompClient.send("/chat/ask", {}, JSON.stringify(chatMessage));
        messageInput.val('');
      }
    },

    _onKeyPress(event) {
      if (event.keyCode === 13) { //Enter
        this.send();
      }
    },
    _onConnected(frame) {
      this.username = frame.headers['user-name'];

      this.stompClient.subscribe('/secured/user/queue/specific-user-' + this.username, this._onMessageReceived.bind(this));
      this.stompClient.subscribe('/secured/seller/activated', this._onMessageReceived.bind(this));

      this.stompClient.send("/chat/user.join", {}, JSON.stringify({ sender: this.username, type: 'JOIN' }));

      this.container.find('.btn-send')
        .prop('disabled', false);
      this.container.find('.connecting').hide();
    },
    _onError(error) {
      const connectingElement = this.container.find('.connecting');
      connectingElement.text('Could not connect to WebSocket server. Please refresh this page to try again!');
      connectingElement.css('color', 'red');
    },
    _onMessageReceived(payload) {
      const message = JSON.parse(payload.body);
      const messageElement = document.createElement('li');
      if (message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
      } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
      } else {
        messageElement.classList.add('chat-message');

        const avatarElement = document.createElement('i');
        const avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = this._getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        const usernameElement = document.createElement('span');
        const usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
      }

      const textElement = document.createElement('p');
      const messageText = document.createTextNode(message.content);
      textElement.appendChild(messageText);

      messageElement.appendChild(textElement);

      const messageArea = this.container.find('.messageArea')[0];
      messageArea.appendChild(messageElement);
      messageArea.scrollTop = messageArea.scrollHeight;
    },
    _getAvatarColor(messageSender) {
      let hash = 0;
      for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
      }
      const index = Math.abs(hash % utils.colors.length);
      return utils.colors[index];
    }
  }

  $(document).ready(() => {
    new Customer().init();
  });
})(jQuery, window.UTILS, window.CONTEXT_PATH);