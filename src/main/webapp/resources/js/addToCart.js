"use strict"; 

(function ($, utils, contextPath){
	const App = {
	init: function(){
		//this.contextRoot = "/" + window.location.pathname.split( '/' )[1];
		this.contextRoot = contextPath;
		this.orderNow=$('#orderNow');
		
		this.bindEvents();
	},
	bindEvents: function(){
		this.orderNow.on("click", this.addToCart.bind(this));
	},

	addToCart: function(e){
		e.preventDefault();
		const productId=utils.getParameterByName("id");
		//console.log(this.productId);
		const urlToRest = '/api/cart/add/';

		$.ajax({
			type: 'PUT',
			url: this.contextRoot + urlToRest + productId,
		success: (data) => {
			alert("Product added to the Cart successfully!");
		},
		error: (errorObject) => {	
			console.log("error: " + errorObject);
				 		
		}
	})
	}

	};
	App.init();
})(jQuery, UTILS, window.CONTEXT_PATH);



