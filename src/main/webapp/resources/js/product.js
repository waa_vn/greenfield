var base = '/' + location.pathname.split("/")[1];

$(function () {
	Product.init();
})

// Template generators
var template = {

	oneRow: function (product) {
		var productImage = product.imageUrl?`<img style="width:64px" src="${base}/${product.imageUrl}" />`:'';
		return `<tr>
			<td>${product.productId}</td>
			<td>${productImage}</td>
			<td>${product.name}</td>
			<td>${product.description}</td>
			<td>${product.unitPrice}</td>
			<td>${product.unitsInStock}</td>
			<td>${product.unitsInOrder}</td>
			<td><button onClick="Product.onEdit(${product.id})">Edit</button></td>
		</tr>`;
	},

	table: function (response) {
		var body = '';
		$.each(response, function (i, product) {
			body += template.oneRow(product);
		})
		return body;
	}
}

// Main app for product
var Product = {
	init : function () {

		$("#addButton").on("click", function () {
			Product.onAdd();
		});

		$("#saveProduct").on("click", function () {
			Product.onSave();
		});
	
		Product.loadList();
	
		Product.productForm = document.getElementById("productForm");
	},
	loadList: function () {
		$.ajax({
			url: base + '/api/products',
			type: 'GET',
			dataType: 'json',
			success: function (res) {
				console.log(res);
				var html = template.table(res);
				$("#list").html(html);
			},

			error: function (err) {
				const jsonErr = err.responseJSON
				if (jsonErr.type=="ValidationError"){

					$('#errors').html("");
					$("#errors").append( '<H3 align="center"> Error(s)!! <H3>');
					$("#result").append( '<p>');

					$.each(jsonErr.errors, function(i, err){
						$('#errors').append(err.message + '<br>');
					})

					$("#errors").append( '</p>');

					make_visible('errors')
					make_hidden('formInput')
				}else{
					console.log(error.message || err.responseText)
				}

				console.log(err);

			}

		});
	},
	onAdd: function () {
		$("#productForm").trigger("reset");
		$("#popup").show();
	},
	onEdit: function (id) {
		Utils.clearFormError(Product.productForm);
		$.ajax({
			url: base + '/api/products/' + id,
			type: 'GET',
			dataType: 'json',
			success: function (resp) {
				console.log(resp);
				Product.productForm.reset();
				resp.category_id = (resp.category && resp.category.id!=null)?resp.category.id:'';
				Utils.populate(Product.productForm, resp);
				$("#popup").show();
				Utils.goToForm();
			},
			error: function (err) {
				console.log(err);
			}
		});


	},
	onSave: function () {

		Utils.clearFormError(Product.productForm);
		// this.saveImage().then(function(imageUrl){
			var data = Utils.serializeObject($('#productForm'));
			data.category = data.category_id!=""?{id:data.category_id}: null;
			$.ajax({
				url: base + '/api/products/save',
				type: 'POST',
				// enctype: 'multipart/form-data',
				dataType: 'json',
				data: JSON.stringify(data),
				contentType: 'application/json',

				success: function (res) {
					console.log(res);


					Product.loadList();

					Utils.showInfo("Product Saved");
				},

				error: function (err) {
					var jsonErr = err.responseJSON;
					if (jsonErr.type=="ValidationError"){


						$.each(jsonErr.errors, function(i, err){
							Utils.showFieldErrors(Product.productForm, err.fieldName, err.message);
						});

					}else{
						Utils.showInfo(jsonErr.message || err.responseText);
						console.log(jsonErr.message || err.responseText);
					}


					console.log(err);

				}

			});
		// }).catch(function(e){
		// 	console.log(e);
		// });
	},
	onUpload: function(){
		// return new Promise(function(rs, rj){

			var form = Product.productForm;

			var data = new FormData(form);
			$.ajax({
				type: "POST",
				enctype: 'multipart/form-data',
				url: base + "/api/upload",
				data: data,
				dataType: 'json',
				//http://api.jquery.com/jQuery.ajax/
				//https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
				processData: false, //prevent jQuery from automatically transforming the data into a query string
				contentType: false,
				cache: false,
				timeout: 600000,
				success: function (data) {

					console.log(data);
					$(Product.productForm).find("#imageUrl").val(data && data.filePath.replace(/\\/g,"/"));
					// rs(data);

				},
				error: function (err) {
					console.log("ERROR : ", err);
					var jsonErr = err.responseJSON;
					if (jsonErr.type=="ValidationError"){


						$.each(jsonErr.errors, function(i, err){
							Utils.showFieldErrors(Product.productForm, err.fieldName, err.message);
						});

					}else{
						Utils.showInfo(jsonErr.message || err.responseText);
						console.log(jsonErr.message || err.responseText);
					}


					console.log(err);
				}
			});
		// });

		
	}
};

var Utils = {
	populate: function populate(frm, data) {
		$.each(data, function (key, value) {
			var ctrl = $('[name=' + key + ']', frm);
			switch (ctrl.prop("type")) {
				case "radio": case "checkbox":
					ctrl.each(function () {
						if ($(this).attr('value') == new String(value)) $(this).attr("checked", "checked");
					});
					break;
				default:
					ctrl.val(value);
			}
		})
	},

	toggle: function (id) {
		var element = document.getElementById(id);
		if (element.style.display == 'block')
			element.style.display = 'none';
		else
			element.style.display = 'block';
	},

	hide: function (id) {
		var element = document.getElementById(id);
		element.style.display = 'none';
	},

	goToForm: function (){
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#popup").offset().top
		}, 1000);
	},

	showFieldErrors: function (form, fid, errorMsg) {
		$(form).find("#"+fid).next("span.error").html(errorMsg).show();
		
	},

	clearFormError: function(form){
		$(form).find("span.error").hide();
		Utils.hideInfo();
	},

	showInfo: function(msg){
		$("#info").html(msg).show();
		Utils.goToForm();
	},

	hideInfo: function(){
		$("#info").html("").hide();
	},


	resetForm: function (id) {
		var element = document.getElementById(id);
		element.reset();

	},


	// Translate form to array
	// Then put in JSON format
	serializeObject: function (form) {
		var jsonObject = {};
		var array = form.serializeArray();
		$.each(array, function () {
			jsonObject[this.name] = this.value;
		});
		return jsonObject;

	}
};

