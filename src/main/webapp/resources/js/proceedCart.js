"use strict"; 

(function ($, utils, contextPath){
	const App = {
	init: function(){
		//this.contextRoot = "/" + window.location.pathname.split( '/' )[1];
		this.contextRoot = contextPath;
		this.cartId=utils.getParameterByName("cartId");
		this.initCart=$('#initCart');
		this.cartInfo=$('#cartInfo');
		this.table=$('#cartData');
		
		this.initCartId();
		this.bindEvents();
	},
	bindEvents: function(){
		//event by from form load if any
	},
	initCartId: function(){
		this.refreshCart(this.cartId);
	},
	refreshCart: function(cartId){
		const urlToRest = '/api/cart/';
		$.ajax({
			type: 'GET',
			url: this.contextRoot + urlToRest + cartId,
			dataType: 'json',
			
		success: (data) => {
			//render cart data to cart.jsp
			this.renderCartView(data);
		},
		error: (errorObject) => {	
			console.log("error: " + errorObject);
			if (errorObject.responseText==""){
				$("#cartStatus").html("<h3>Cart is empty</h3>");
			}
		}
	})
	},
	renderCartView: function(data){
		
		const cartItems = data.cartItems;
		const numberOfProduct = Object.values(cartItems).length;
		const cartGrandTotal = data.grandTotal;
        // Fill cart info
		let tableBody= "";
		this.cartInfo.empty();
		//display status of cart empty or not
		if (numberOfProduct>0) {
			$("#cartStatus").html("<h3>" + numberOfProduct + " product(s) in your cart</h3>");
			tableBody += `<a href="#" id="checkout">Checkout</a>`;
		} else {
			$("#cartStatus").html("<h3>Cart is empty</h3>");
		}
		//Fill in cart table and its contents
		tableBody += `<table class="table" id="cartData">`;
		tableBody +=`<thead>
							<tr>
								<th>Product</th>
								<th>Unit price</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Action</th>
							</tr>
						</thead>
				`;
		
		for(const item of Object.values(cartItems)) {
		//items added displaying
			tableBody += `<tr>
						<td> ${item.product.productId} - ${item.product.name} </td>
						<td> ${item.product.unitPrice}$ </td>
						<td> ${item.quantity} </td>
						<td> ${item.totalPrice}$ </td>
						<td><a href='#' id='${item.product.productId}' class='removeCart'>Remove</a></td>
						</tr>
						`;

		}
		
			//Grand Total displaying
			tableBody += `<tr>
						<th></th>
						<th></th>
						<th>Grand Total</th>
						<th>${cartGrandTotal}$</th>
						<th></th>
						</tr>
						`;
		
			tableBody += `</table>`;			
		this.cartInfo.append(tableBody);

		//Bind event for remove with id
		this.cartInfo.on("click", '.removeCart', this.removeFromCart.bind(this));
		if (numberOfProduct>0) {
			$("#checkout").on("click", this.checkout.bind(this));
		}
        
	},
	checkout: function(){
		window.location = this.contextRoot + '/checkout?cartId=' + this.cartId;
	},
	removeFromCart: function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		if (confirm('Are you sure to remove?')==true) {
			
			const productId = e.target.id;
			const urlToRest = '/api/cart/remove/';
			$.ajax({
				type: 'PUT',
				url: this.contextRoot + urlToRest + productId,
				dataType: 'json',
				contentType: "application/json",
			success: (data) => {
				this.refreshCart(this.cartId);
				alert("Product removed in cart successfully!");
			},
			error: (errorObject) => {	
				console.log("error: " + errorObject);
							
			}
		})
	}}

	};
	App.init();
})(jQuery, UTILS, window.CONTEXT_PATH);


    



