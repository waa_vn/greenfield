"use strict"; 

(function ($, utils, contextPath){
	const App = {
	init: function(){
		//this.contextRoot = "/" + window.location.pathname.split( '/' )[1];
		this.contextRoot = contextPath;
		this.autoLoad=$("#autoLoad");
		//fields in collectionShippingDetail page
		this.name=$("#name");
		this.streetName=$("#streetName");
		this.state=$("#state");
		this.country=$("#country");
		this.zipCode=$("#zipCode");
		//save previous values
		this.nameValue=this.name.val();
		this.streetNameValue=this.streetName.val();
		this.stateValue=this.state.val();
		this.countryValue=this.country.val();
		this.zipCodeValue=this.zipCode.val();
		
		this.bindEvents();
	},
	bindEvents: function(){
		this.autoLoad.on("change", this.populate.bind(this));
	},
	populate: function(e){
		if (this.autoLoad.is(':checked') == true) {
			this.name.val(this.nameValue);
			this.streetName.val(this.streetNameValue);
			this.state.val(this.stateValue);
			this.country.val(this.countryValue);
			this.zipCode.val(this.zipCodeValue);
    } 	else {
       		this.name.val('');
			this.streetName.val('');
			this.state.val('');
			this.country.val('');
			this.zipCode.val('');
    }
	}

	};
	App.init();
})(jQuery, UTILS, window.CONTEXT_PATH);



