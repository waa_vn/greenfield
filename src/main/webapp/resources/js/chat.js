(function($, utils, contextPath) {
  'use strict';
  function autoHeightAnimate(element, time, autoHeight, autoWidth){
    let curHeight = element.height();
    let curWidth = element.width();
    element.height(curHeight);
    element.width(curWidth);
    element.stop().animate({ height: autoHeight, width: autoWidth }, time); // Animate to Auto Height
  }

  function Chat() {
    this.chatContainer = $('<div class="chatWithUs"></div>');
    this.chatContainer.appendTo(document.body)
    this.expanded = false;
  }
  Chat.prototype = {
    init() {
      this.chatContainer.css('z-index', 100);
      this._createChatHeader();
      this._createChatContent();
    },
    toggle(event) {
      if (this.expanded) {
        this.header.find('.comment').show();
        this.header.find('.title').hide();
        this.content.hide();
        autoHeightAnimate(this.chatContainer, 500, 40, 40);
        this.content.find('iframe').removeAttr('src');
      } else {
        this.header.find('.comment').hide();
        this.header.find('.title').show();
        this.content.show();
        autoHeightAnimate(this.chatContainer, 500, 400, 600);
        this.content.find('iframe').attr('src', contextPath+'/chat');
      }
      this.expanded = !this.expanded;
    },
    
    _createChatHeader() {
      this.header = $(
        '<div class="header">'+
          '<img class="comment" src="'+ contextPath +'/resources/images/comment.svg"/>'+
          '<div class="title" style="display:none">'+
            'Chat window'+
            '<img class="close" src="'+ contextPath +'/resources/images/close.svg"/>'+
          '</div>'+
        '</div>'
      );
      this.chatContainer.append(this.header);
      this.header.find('.comment').on('click', this.toggle.bind(this));
      this.header.find('.close').on('click', this.toggle.bind(this));
    },
    _createChatContent() {
      this.content = $('<div class="content"><iframe/></div>');
      this.chatContainer.append(this.content);
      this.content.hide();
    }
  }

  $(document).ready(function() {
    new Chat().init();
  });
})(jQuery, window.UTILS, window.CONTEXT_PATH);