"use strict";

window.UTILS = (function () {
	let today = new Date();

	const currentDate = function() {
		return formatDateByUs(today);
	};
	const formatDateByUs = function(d) {
		const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
		const mo = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(d)
		const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
		return `${mo}-${da}-${ye}`;
	};

	const serializeObject = function (form) {
		var jsonObject = {};
		var array = form.serializeArray();
		$.each(array, function() {
				jsonObject[this.name] = this.value;
		});
		return jsonObject;

	};

	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	};

	const colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
	];
	
  function createUUID() {
    let dt = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

	
	return {
		colors: colors,
		createUUID: createUUID,
		currentDate: currentDate,
		formatDateByUs: formatDateByUs,
		getParameterByName: getParameterByName,
		serializeObject: serializeObject
	  }
})();