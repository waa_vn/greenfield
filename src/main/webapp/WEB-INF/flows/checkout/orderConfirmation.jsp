<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

		<section>
			<div>
				<div class="container">
					<h1><spring:message code="cart.orderConfirmation.order"/></h1>
					<h3><spring:message code="cart.orderConfirmation.orderConfirmation"/></h3>
				</div>
			</div>
		</section>
		<div class="container">
			<div class="row">
				<form:form modelAttribute="order" class="form-horizontal">
					<input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}" />

					<fieldset>
						<legend><spring:message code="cart.orderConfirmation.shippingDetails"/></legend>

						<div>
							<p class="floatRight">
								<c:set value="${order.shippingDetail.shippingDate}" var="shippingDate" />
								<c:choose>
									<c:when test="${not empty shippingDate}">
										<b><spring:message code="cart.orderConfirmation.shippingDate"/></b>
										<fmt:formatDate pattern="MM/dd/yyyy"
											value="${order.shippingDetail.shippingDate}" />
									</c:when>
									<c:otherwise>
										<jsp:useBean id="now" class="java.util.Date" />
										<b><spring:message code="cart.orderConfirmation.shippingDate"/></b> <spring:message code="cart.orderConfirmation.shippingEstimate"/> <fmt:formatDate
											pattern="MM/dd/yyyy" value="${now}" />
									</c:otherwise>
								</c:choose>

							</p>
						</div>
						<br />
						<div>
							<div>
								<address>
									<b><spring:message code="cart.orderConfirmation.shippingAddress"/></b> <br> ${order.shippingDetail.name}<br>
									${order.shippingDetail.shippingAddress.streetName}<br>
									${order.shippingDetail.shippingAddress.state}
									,${order.shippingDetail.shippingAddress.zipCode}<br>
									${order.shippingDetail.shippingAddress.country}<br>
								</address>
							</div>

						</div>
						<br />
						<div>
							<div>
								<address>
									<b><spring:message code="cart.orderConfirmation.billingAddress"/></b> <br> ${order.customer.name}<br>
									${order.customer.billingAddress.streetName}<br>
									${order.customer.billingAddress.state},${order.customer.billingAddress.zipCode}<br>
									${order.customer.billingAddress.country}<br> <spring:message code="cart.getCusterInfo.phone"/>
									${order.customer.phoneNumber.area}-${order.customer.phoneNumber.prefix}-${order.customer.phoneNumber.number}
								</address>
							</div>

						</div>

						<div>
							<br />
							<table>
								<thead>
									<tr>
										<th><spring:message code="cart.orderConfirmation.product"/></th>
										<th><spring:message code="cart.orderConfirmation.quantity"/></th>
										<th class="text-center"><spring:message code="cart.orderConfirmation.price"/></th>
										<th class="text-center"><spring:message code="cart.orderConfirmation.total"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="cartItem" items="${order.cart.cartItems}">
										<tr>
											<td>${cartItem.value.product.name}</td>
											<td class="text-center">${cartItem.value.quantity}</td>
											<td>$${cartItem.value.product.unitPrice}</td>
											<td>$${cartItem.value.totalPrice}</td>
										</tr>
									</c:forEach>

									<tr>
										<td> </td>
										<td> </td>
										<td class="text-right"><h4>
												<strong><spring:message code="cart.orderConfirmation.grandTotal"/></strong>
											</h4></td>
										<td class="text-center"><h4>
												<b class="green">$${order.cart.grandTotal}</b>
											</h4></td>
									</tr>
								</tbody>
							</table>

							<p>
								<button id="back" name="_eventId_backToCollectShippingDetail"><spring:message code="cart.button.back"/></button>
								<button type="submit" name="_eventId_orderConfirmed"><spring:message code="cart.button.confirm"/></button>
								<button id="btnCancel" name="_eventId_checkoutCancelled"><spring:message code="cart.button.cancel"/></button>
							</p>
						</div>
					</fieldset>
				</form:form>
			</div>
		</div>

