<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

	<section>
		<div>
			<div class="container">
				<h1>Check out is cancelled</h1>
				<h3>Your Check out is cancelled! Please continue shopping again</h3>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<p>
				<a href="<spring:url value="/products" />" >
					products
				</a>
			</p>
		</div>
		
	</section>
	
