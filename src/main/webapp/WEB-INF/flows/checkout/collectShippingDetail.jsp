<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

	<section>
		<div>
			<div class="container">
				<h1><spring:message code="cart.getShippingAddress.shipping"/></h1>
				<h3><spring:message code="cart.getShippingAddress.shippingDetails"/></h3>
			</div>
		</div>
	</section>
	<section class="container">
		<form:form modelAttribute="order.shippingDetail">
			<fieldset>
				<legend><spring:message code="cart.getShippingAddress.shippingDetails"/></legend>
				
				<p>
		  		     <form:errors path="*" cssStyle="color : red;" /> 
		        </p>
		        
		        <div>
			        <input type="checkbox" id="autoLoad" name="autoLoad" checked="checked">
	  				<label for="autoLoad"><spring:message code="cart.getShippingAddress.loadInfo"/></label>
  				</div>
		        
 				<div class="form-group">
					<label for="name" ><spring:message code="cart.getShippingAddress.name"/></label>
					<div >
						<form:input id="name" path="name" type="text" value="${order.customer.name}" />
						<form:errors path="name" cssStyle="color : red;" /> 
					</div>
				</div>

				<div class="form-group">
					<label for="shippingDate"><spring:message code="cart.getShippingAddress.shipDate"/></label>
					<div>
						<form:input id="shippingDate" path="shippingDate" type="text" />
						<form:errors path="shippingDate" cssStyle="color : red;" /> 
					</div>
				</div>

 
				<div class="form-group">
					<label for="streetName"><spring:message code="cart.getShippingAddress.street"/></label>
					<div>
						<form:input id="streetName" path="shippingAddress.streetName" type="text" value="${order.customer.billingAddress.streetName}"/>
						<form:errors path="shippingAddress.streetName" cssStyle="color : red;" />
					</div>
				</div>

 				<div class="form-group">
					<label for="state"><spring:message code="cart.getShippingAddress.state"/></label>
					<div>
						<form:input id="state" path="shippingAddress.state" type="text"	value="${order.customer.billingAddress.state}"/>
						<form:errors path="shippingAddress.state" cssStyle="color : red;" />
					</div>
				</div>

				<div class="form-group">
					<label for="country"><spring:message code="cart.getShippingAddress.country"/></label>
					<div>
						<form:input id="country" path="shippingAddress.country" type="text" value="${order.customer.billingAddress.country}" />
						<form:errors path="shippingAddress.country" cssStyle="color : red;" />
					</div>
				</div>

				<div class="form-group">
					<label for="zipCode"><spring:message code="cart.getShippingAddress.zipCode"/></label>
					<div>
						<form:input id="zipCode" path="shippingAddress.zipCode" type="text"	value="${order.customer.billingAddress.zipCode}" />
						<form:errors path="shippingAddress.zipCode" cssStyle="color : red;" />
					</div>
				</div>

				<input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}"/>

				<br/>
				<div class="form-group">
					<div>
						<button id="back" name="_eventId_backToCollectCustomerInfo"><spring:message code="cart.button.back"/></button>
						<input type="submit" id="btnAdd" value="<spring:message code="cart.button.add"/>" name="_eventId_shippingDetailCollected"/>
						<button id="btnCancel" name="_eventId_checkoutCancelled"><spring:message code="cart.button.cancel"/></button>
					</div>
				</div>

			</fieldset>
		</form:form>
	</section>
