<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

	<section>
		<div>
			<div class="container">
				<h1><spring:message code="cart.getCusterInfo.customer"/></h1>
				<h3><spring:message code="cart.getCusterInfo.customerDetails"/></h3>
			</div>
		</div>
	</section>
	<section class="container">
		<form:form modelAttribute="order.customer">
			<fieldset>
				<legend><spring:message code="cart.getCusterInfo.customerDetails"/></legend>

		  		<p>
		  		     <form:errors path="*" cssStyle="color : red;" /> 
		        </p>
  

				<div class="form-group">
					<label for="customerId" ><spring:message code="cart.getCusterInfo.customerId"/></label>
					<div>
						<form:input id="customerId" path="customerId" type="text"/>
						<form:errors path="customerId" cssStyle="color : red;" /> 
					</div>
				</div>

				<div class="form-group">
					<label for="name"><spring:message code="cart.getCusterInfo.name"/></label>
					<div>
						<form:input id="name" path="name" type="text" />
						<form:errors path="name" cssStyle="color : red;" /> 
					</div>
				</div>
				
				<div class="form-group">
					<label for="email"><spring:message code="cart.getCusterInfo.email"/></label>
					<div>
						<form:input id="email" path="email" type="text" />
						<form:errors path="email" cssStyle="color : red;" /> 
					</div>
				</div>
 
				<div class="form-group">
					<label for="streetName"><spring:message code="cart.getCusterInfo.street"/></label>
					<div>
						<form:input id="streetName" path="billingAddress.streetName" type="text" />
						<form:errors path="billingAddress.streetName" cssStyle="color : red;" /> 
					</div>
				</div>

 				<div class="form-group">
					<label for="state"><spring:message code="cart.getCusterInfo.state"/></label>
					<div>
						<form:input id="state" path="billingAddress.state" type="text" />
						<form:errors path="billingAddress.state" cssStyle="color : red;" />
					</div>
				</div>

				<div class="form-group">
					<label for="country"><spring:message code="cart.getCusterInfo.country"/></label>
					<div>
						<form:input id="country" path="billingAddress.country" type="text" />
						<form:errors path="billingAddress.country" cssStyle="color : red;" />
					</div>
				</div>

				<div class="form-group">
					<label for="zipCode"><spring:message code="cart.getCusterInfo.zipCode"/></label>
					<div>
						<form:input id="zipCode" path="billingAddress.zipCode" type="text" />
						<form:errors path="billingAddress.zipCode" cssStyle="color : red;" />
					</div>
				</div>
				
				
				 <div class="form-group">
					<label for="area"><spring:message code="cart.getCusterInfo.phone"/></label><form:input path="phoneNumber.area" id="area" size="3" /><form:errors path="phoneNumber.area" cssStyle="color : red;" />
					<form:input path="phoneNumber.prefix" id="prefix" size="3" /><form:errors path="phoneNumber.prefix" cssStyle="color : red;" />
					<form:input path="phoneNumber.number" id="number" size="4" /><form:errors path="phoneNumber.number" cssStyle="color : red;" />
				</div> 
				
				<!-- <div>
		            <label for="phone"><spring:message code="cart.getCusterInfo.phone"/></label>
		            <form:input id="phone" path="phoneNumber" placeholder="nnn-nnn-nnnn"/>
       			 </div> -->
				

				<input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}"/>
				
				<br/>
				<div class="form-group">
					<div>
						<input type="submit" id="btnAdd" value="<spring:message code="cart.button.add"/>" name="_eventId_customerInfoCollected" />
						<button id="btnCancel" name="_eventId_checkoutCancelled"><spring:message code="cart.button.cancel"/></button>
					</div>
				</div>

			</fieldset>
		</form:form>
	</section>
	
