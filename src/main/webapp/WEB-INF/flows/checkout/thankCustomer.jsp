<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

		<section>
			<div>
				<div class="container">
					<h1><spring:message code="cart.thankyou.message"/></h1>
					<h3 class="cartStatus">
						<spring:message code="cart.thankyou.orderFinished1"/> <b>${order.orderId}</b> <spring:message code="cart.thankyou.orderFinished2"/> 
						<b><fmt:formatDate pattern="MM/dd/yyyy" value="${order.shippingDetail.shippingDate}" />
						</b>!
					</h3>
				</div>
			</div>
		</section>

		<section>
			<table>
				<thead>
					<tr>
						<th><spring:message code="cart.orderConfirmation.product"/></th>
						<th><spring:message code="cart.orderConfirmation.quantity"/></th>
						<th class="text-center"><spring:message code="cart.orderConfirmation.price"/></th>
						<th class="text-center"><spring:message code="cart.orderConfirmation.total"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="cartItem" items="${order.cart.cartItems}">
						<tr>
							<td>${cartItem.value.product.name}</td>
							<td class="text-center">${cartItem.value.quantity}</td>
							<td>$${cartItem.value.product.unitPrice}</td>
							<td>$${cartItem.value.totalPrice}</td>
						</tr>
					</c:forEach>

					<tr>
						<td> </td>
						<td> </td>
						<td class="text-right"><h4>
								<b><spring:message code="cart.orderConfirmation.grandTotal"/></b>
							</h4></td>
						<td class="text-center"><h4>
								<b class="green">$${order.cart.grandTotal}</b>
							</h4></td>
					</tr>
				</tbody>
			</table>
		</section>

		<section>
			<div class="container">
				<p>
					<a href="<spring:url value="/" />"><spring:message code="cart.link.products"/></a>
				</p>
			</div>

		</section>

