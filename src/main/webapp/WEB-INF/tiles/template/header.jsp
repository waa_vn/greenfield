<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="currentUrl"><%=request.getAttribute("javax.servlet.forward.servlet_path")%></c:set>
<c:set var="localeCode" value="${pageContext.response.locale.toString()}" />

<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
	<a class="navbar-brand" href='<spring:url value="/"/>'><img style="width: 40px; height: 40px;" src='<spring:url value="/resources/images/logo.png"/>'/></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<security:authorize access="isAuthenticated()">
			<ul class="navbar-nav">
				<c:if test='${"/".equals(currentUrl)}'>
					<li class="nav-item active">
				</c:if>
				<c:if test='${!"/".equals(currentUrl)}'>
					<li class="nav-item">
				</c:if>
				<a class="nav-link vertical" href='<spring:url value="/"/>'><spring:message code="Home.menu"/></a></li>
				<security:authorize access="hasRole('ROLE_ADMIN')">
					<li class="nav-item ${currentUrl.startsWith("/admin/product")?"active":""}">
						<a class="nav-link vertical" href='<spring:url value="/admin/product"/>'><spring:message code="Admin.product"  text="Product Manage"/></a></a>
					</li>
				</security:authorize>
				<c:if test='${currentUrl.startsWith("/cart")}'>
					<li class="nav-item active">
				</c:if>
				<c:if test='${!currentUrl.startsWith("/cart")}'>
					<li class="nav-item">
				</c:if>
				<a class="nav-link vertical" href='<spring:url value="/cart/"/>'><spring:message code="Cart.menu"/></a></a></li>
			</ul>
		</security:authorize>
	</div>
	<div class="text-white">
		<c:if test='${localeCode.contains("en")}'>
			<a href="?lang=vi_VN">
				<img src='<spring:url value="/resources/images/vi.svg"/>' alt='<spring:message code="Language.VietNamese.text"/>' class="mr-3" style="width: 24px; height: 24px; cursor: pointer;">
			</a>
		</c:if>

		<c:if test='${!localeCode.contains("en")}'>
			<a href="?lang=en_US">
				<img src='<spring:url value="/resources/images/us.svg"/>' alt='<spring:message code="Language.English.text"/>' class="mr-3" style="width: 24px; height: 24px; cursor: pointer;">
			</a>
		</c:if>
		<spring:message code="Welcome.text"/>, 
		<security:authorize access="isAuthenticated()">
			<security:authentication property="principal.username" />
		</security:authorize>
		<security:authorize access="!isAuthenticated()"><spring:message code="Guest.text"/></security:authorize>
		<security:authorize access="!isAuthenticated()">
			<a class="text-white ml-3" href='<spring:url value="/login"/>'><spring:message code="Login.text"/></a>
		</security:authorize>
		<security:authorize access="isAuthenticated()">
			<a class="text-white ml-3" href='<spring:url value="/logout"/>'><spring:message code="Logout.text"/></a>
		</security:authorize>
	</div>
</nav>