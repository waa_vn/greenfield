<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<c:set var="title"><tiles:getAsString name="title" /></c:set>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href='<spring:url value="/resources/css/site.css"></spring:url>' type="text/css" rel="stylesheet" />
	<title><spring:message code="${title}" text="${title}"/></title>
	<tiles:importAttribute name="stylesheets"/>
	<c:forEach items="${stylesheets}" var="item">
		<link href='<spring:url value="${item}"/>' type="text/css" rel="stylesheet"/>
	</c:forEach>
</head>

<body>
	<noscript>
		<h2>Sorry! Your browser doesn't support Javascript</h2>
	</noscript>

	<tiles:insertAttribute name="header" />

	<div class="container" style="margin-bottom: 80px;">
		<tiles:insertAttribute name="body" />
	</div>

	<tiles:insertAttribute name="footer" />

	<script>
		window.CONTEXT_PATH = '${pageContext.request.contextPath}'
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src='<spring:url value="/resources/js/utils.js"></spring:url>'></script>
	<security:authorize access="isAuthenticated()">
		<script src='<spring:url value="/resources/js/chat.js"></spring:url>'></script>
	</security:authorize>
	<tiles:importAttribute name="scripts"/>
	<c:forEach items="${scripts}" var="item">
		<script src='<spring:url value="${item}"/>'></script>
	</c:forEach>
</body>
</html>
