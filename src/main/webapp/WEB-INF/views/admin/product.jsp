<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

  <%-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script> --%>
  

<tiles:insertDefinition name="baseLayout">

	<tiles:putAttribute name="title">Product Manage</tiles:putAttribute>

	<tiles:putAttribute name="stylesheets">
		/resources/css/cart.css
	</tiles:putAttribute>
	<tiles:putAttribute name="scripts">
		/resources/js/product.js
	</tiles:putAttribute>

  <tiles:putAttribute name="body">
  <section id="main">
    <div>
      <table class="table table-striped">
        <thead class="thead-dark">
          <th scope="col"><spring:message code="productId" /> </th>
		  <th scope="col"><spring:message code="imageUrl" /> </th>
          <th scope="col"><spring:message code="name" /> </th>
          <th scope="col"><spring:message code="description" /> </th>
          <th scope="col"><spring:message code="unitPrice" /> </th>
          <th scope="col"><spring:message code="unitsInStock" /> </th>
          <th scope="col"><spring:message code="unitsInOrder" /> </th>
		  <th scope="col"><spring:message code="action" text="Actions" /> </th>
        </thead>
        <tbody id="list"></tbody>
      </table>
    </div>
    <div>
      <button id="addButton">Add Product</button>
    </div>

    
  </section>
	<br>
  <div id="popup" style="display:none">
    <h2 id="formTitle">Product:</h2>
	<div><h4 id="info" class="alert-success"></h4></div>
    <form id="productForm" method="post" enctype="multipart/form-data">
	
      <input type="hidden" name="id">
	  <div class="form-group row">
	  	<label for="productId" class="col-sm-2 col-form-label">Product Id: </label>
		<div class="col-sm-4">
			<input type="text" name="productId" id="productId" value="P-" maxlength="7" class="form-control"/>
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	  </div>
       <div class="form-group row">
	  	<label for="name" class="col-sm-2 col-form-label">Name: </label>
		<div class="col-sm-10">
		  <input type="text" name="name" id="name" class="form-control"/>
		  <span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Price:  </label>
		<div class="col-sm-10">
	  		<input type="text" name="unitPrice" id="unitPrice" class="form-control"/>
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>

	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Condition: </label>
		<div class="col-sm-10">
			<input type="text" name="inCondition" id="inCondition" class="form-control"/>
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Category:</label>
		<div class="col-sm-10">
			<select name="category_id" class="form-control">
					<option value=""></option>
				<c:forEach items="${categories}" var="cat" >
					<option value="${cat.id}">${cat.name}</option>	
				</c:forEach>
			</select>

		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Description:</label>
		<div class="col-sm-10">
			<textarea type="text" name="description" class="form-control"></textarea>
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Discontinued:</label>
		<div class="col-sm-10">
			<label for="no">No</label> <input id="no" type="radio" name="discontinued" selected="selected" value="false" />
			<label for="yes">Yes </label><input id="yes" type="radio" name="discontinued" value="true"/>
		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Manufacturer:</label>
		<div class="col-sm-10">
			<input type="text" name="manufacturer" class="form-control" />
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>
	<div class="form-group row">
    	<label for="productId" class="col-sm-2 col-form-label">UnitsInOrder:</label>
		<div class="col-sm-10">
			<input type="text" name="unitsInOrder" readonly class="form-control" />
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">UnitsInStock:</label>
		<div class="col-sm-10">
			<input type="text" name="unitsInStock" class="form-control" />
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
	</div>
	<div class="form-group row">
		<label for="productId" class="col-sm-2 col-form-label">Image: </label>
		<div class="col-sm-10">
			<input type="text" id="imageUrl" name="imageUrl" class="form-control col-sm-5" readonly style="display: inline-block"/>
			<input type="file" name="productImage" class="form-control col-sm-5" accept="image/*" style="display: inline-block; border: none"/>
			<input type="button" value="Upload" onClick="Product.onUpload()" />
			<span class="error text-danger" style="display:none">Something may have gone wrong</span>
		</div>
     </div> 
		
	  <%-- <input type="button" value="Save" onClick="Product.onSave()" /> --%>
      <button id="saveProduct" type="button">Save Product</button>
    </form>
  </div>
	</tiles:putAttribute>
</tiles:insertDefinition>
