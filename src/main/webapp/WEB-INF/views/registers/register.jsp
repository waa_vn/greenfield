<%--
  Created by IntelliJ IDEA.
  User: vtrang
  Date: 6/13/20
  Time: 3:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

Register:
<form:form action="" modelAttribute="user" method="post">
    <div>
        <form:errors path="*" cssStyle="color: red;"/>
    </div>
    <fieldset>
        <label for="username">Username: </label>
        <input type="text" id="username" name="username" size="50" />
        <br />

        <label for="password">Password: </label>
        <input type="password" id="password" name="password" size="50" />
        <br />

        <label for="verifyPassword">Verify Password: </label>
        <input type="password" id="verifyPassword" name="verifyPassword" size="50" />
        <br />

        <form:hidden path="enabled" name="enabled" value="1"></form:hidden>
        <security:csrfInput />

        <input type="submit" value="Submit" />
    </fieldset>
</form:form>