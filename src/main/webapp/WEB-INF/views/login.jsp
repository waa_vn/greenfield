<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<c:if test="${not empty error}">
  <div>
    <spring:message code="AbstractUserDetailsAuthenticationProvider.badCredentials" /><br />
  </div>
</c:if>
Login:
<form:form action="login" method="post">
  <label for="username">Username: </label>
  <input type="text" name="username" id="username" size="50" /><br />

  <label for="password">Password: </label>
  <input type="password" name="password" id="password" size="50" /><br />
  <br />

  <input type='checkbox' name="keepMe" />Remember Me? <br />
  <br />

  <input type="submit" value="Log In" />
  <security:csrfInput />
  <p>
    Not a member? <a href="<spring:url value='register'></spring:url>">Register</a>
  </p>
</form:form>
