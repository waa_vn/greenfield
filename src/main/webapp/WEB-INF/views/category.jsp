<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<%-- 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

 	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="<spring:url value="/resource/js/cart.js"/>"></script>
	
<title>Category - ${category.name}</title>
</head>
<body> --%>


<tiles:insertDefinition name="baseLayout">

	<%-- <tiles:putAttribute name="stylesheets">
		/resource/css/cart.css"
	</tiles:putAttribute> --%>
	<tiles:putAttribute name="scripts">
		/resources/js/addToCart.js
	</tiles:putAttribute>

	<tiles:putAttribute name="title">${product.name} - Product Details</tiles:putAttribute>
<%-- </tiles:insertDefinition> --%>

	
<tiles:putAttribute name="body">
	<link rel="stylesheet"	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<h1> ${category.name} </h1>
 	<section class="container" >
		
		<c:forEach items="${products}" var="product" >
			<div class="row">
				<div class="col-md-5">
					<img src="<c:url value='/${product.imageUrl}'></c:url>" alt="image"  style = "width:100%"/>
				</div>
			
				<div class="col-md-5">
					<h3><a href="<c:url value='/product?id=${product.productId}'></c:url>"> ${product.name}</a></h3>
					<p>${product.description}</p>
					<p>
						<strong>Item Code : </strong><span class="label label-warning">${product.productId}</span>
					</p>
					<p>
						<strong>Manufacturer</strong> : ${product.manufacturer}
					</p>
					 
					<p>
						<strong>Condition</strong> : ${product.inCondition}
					</p>
					<p>
						<strong>In stock </strong> : ${product.unitsInStock}
					</p>
					<h4>${product.unitPrice} USD</h4>
					<p  >
						<a href="<c:url value='/product?id=${product.productId}'></c:url>" class="btn btn-warning btn-large"> 
	<span class="glyphicon-hand-right glyphicon"></span> Detail </a>
	<a href="<spring:url value="/cart" />" class="btn btn-default">
		<span class="glyphicon-hand-right glyphicon"></span> View Cart
	</a>


	<%-- <a href="<spring:url value="/" />" class="btn btn-default">
							<span class="glyphicon-hand-left glyphicon"></span> Back
						</a> --%>

					</p>

				</div>
			</div>
		</c:forEach>
<!--/div-->
	</section>
</tiles:putAttribute>
</tiles:insertDefinition>	
<%-- </body>
</html> --%>
