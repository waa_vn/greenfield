<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<section>
	<div>
		<div class="container">
			<h1>Cart</h1>
			<div id="cartStatus" class="cartStatus"></div>
		</div>
	</div>
</section>

<section class="container">
	<div id="initCart">
		<div id="cartInfo"></div>

		<a href="<spring:url value="/" />">Continue shopping </a>
	</div>
</section>