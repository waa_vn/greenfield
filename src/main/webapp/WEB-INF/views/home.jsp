<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="row banner">
</div>
<div class="row mt-3">
  <jsp:include page="category-menu.jsp">
    <jsp:param name="categoryMenuId" value="categoryMenuId" />
  </jsp:include>
  <div class="col-9 no-padding">
    <c:forEach items="${categories}" var="category">
      <ul class="list-group">
        <li class="list-group-item header active">${category.name}</li>
        <c:if test="${!(empty category.products)}">
          <li class="list-group-item">
            <ul class="list-product no-padding">
              <c:forEach items="${category.products}" var="product">
                <li class="product">
                  <div class="card">
                    <img class="card-img-top" src="${product.imageUrl}" alt="${product.name}">
                    <div class="card-body">
                      <h5 class="card-title">${product.name}</h5>
                      <p class="card-text">${product.description}</p>
                      <a href='<spring:url value="/product?id=${product.productId}"/>' class="btn btn-primary">Go detail</a>
                    </div>
                  </div>
                </li>
              </c:forEach>
            </ul>
          </li>
        </c:if>
        <c:if test="${empty category.products}">
          <li class="list-group-item">No product in this category.</li>
        </c:if>
      </ul>
    </c:forEach>
  </div>
</div>