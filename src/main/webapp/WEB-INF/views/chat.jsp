<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
  <title>Chat Page</title>
  <link rel="stylesheet" href='<spring:url value="/resources/css/chat.css"></spring:url>' />
</head>

<body>
  <noscript>
    <h2>Sorry! Your browser doesn't support Javascript</h2>
  </noscript>
  <security:authorize access="!hasRole('ROLE_ADMIN')">
    <div id="customer" style="height: 100%;">
      <div class="connecting">
        Connecting...
      </div>
      <ul class="messageArea" style="height: calc(100% - 46px);"></ul>
      <div class="form-group">
        <div class="input-group clearfix">
          <input type="text" placeholder="Type a message..." autocomplete="off" style="margin-left: 4px;width: calc(100% - 77px);" class="form-control inputMessage" />
          <button type="button" class="primary btn-send">Send</button>
        </div>
      </div>
    </div>
  </security:authorize>
  <security:authorize access="hasRole('ROLE_ADMIN')">
    <div id="seller" style="height: 100%;">
      <div class="connecting">
        Connecting...
      </div>
      <div style="display: flex; height: 100%;">
        <ul class="customer-list"></ul>
        <div class="message-wrapper">
          <ul class="messageArea"></ul>
          <div class="form-group">
            <div class="input-group clearfix">
              <input type="text" placeholder="Type a message..." autocomplete="off" style="width: calc(100% - 77px); margin-left: 2px;" class="form-control inputMessage" />
              <button type="button" class="primary btn-send">Send</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </security:authorize>

  <script>
		window.CONTEXT_PATH = '${pageContext.request.contextPath}'
  </script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
  <script src='<spring:url value="/resources/js/utils.js"></spring:url>'></script>
  <security:authorize access="!hasRole('ROLE_ADMIN')">
    <script src='<spring:url value="/resources/js/customer.js"></spring:url>'></script>
  </security:authorize>
  <security:authorize access="hasRole('ROLE_ADMIN')">
    <script src='<spring:url value="/resources/js/seller.js"></spring:url>'></script>
  </security:authorize>
</body>

</html>