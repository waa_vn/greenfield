<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<tiles:insertDefinition name="baseLayout">

	<%-- <tiles:putAttribute name="stylesheets">
		/resource/css/cart.css"
	</tiles:putAttribute> --%>
	<tiles:putAttribute name="scripts">
		/resources/js/addToCart.js
	</tiles:putAttribute>

	<tiles:putAttribute name="title">${product.name} - Product Details</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
 	<section class="container" >
		<h1>${product.name}</h1>
		<div class="row">
		<div class="col-md-5">
	<img src="<c:url value='${product.imageUrl}'></c:url>" alt="image"  style = "width:100%"/>
        </div>
		
			<div class="col-md-5">
				
				<p>${product.description}</p>
				<p>
					<strong>Item Code : </strong><span class="label label-warning">${product.productId}</span>
				</p>
				<p>
					<strong>Manufacturer</strong> : ${product.manufacturer}
				</p>
				<p>
					<strong>Category</strong> : <a href="<c:url value='/category/${product.category.id}'/>"> ${product.category} </a>
				</p>
				<p>
					<strong>Condition</strong> : ${product.inCondition}
				</p>
				<p>
					<strong>In stock </strong> : ${product.unitsInStock}
				</p>
				<h4>$${product.unitPrice}</h4>
				<p>
				<!-- call ajax addToCart -->
				<p  >
						<a href="#" class="btn btn-warning btn-large" id="orderNow"> 
						<%-- onclick="addToCart('${product.productId}')">  --%>
						<span class="glyphicon-shopping-cart glyphicon"></span> Order Now </a>
						<a href="<spring:url value="/cart" />" class="btn btn-default">
							<span class="glyphicon-hand-right glyphicon"></span> View Cart
						</a>

						<a href="<spring:url value="/" />" class="btn btn-default">
							<span class="glyphicon-hand-left glyphicon"></span>Back
						</a>

				</p>

			</div>
		</div>
<!--/div-->
	</section>

	</tiles:putAttribute>
</tiles:insertDefinition>
