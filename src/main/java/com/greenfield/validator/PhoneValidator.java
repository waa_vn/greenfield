package com.greenfield.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;

import com.greenfield.domain.Phone;

@Component
public class PhoneValidator implements ConstraintValidator<PhoneAnotation, Phone> {

	@Override
	public boolean isValid(Phone phone, ConstraintValidatorContext context) {
		//System.out.println("+++ : " + phone);
		if (phone==null || phone.getArea()==null || phone.getNumber()==null || phone.getPrefix()==null) {
			return false;
		}
		
		if (phone.getArea()<100 || phone.getArea()>999) {
			return false;
		}
		if (phone.getPrefix()<100 || phone.getPrefix()>999) {
			return false;
		}
		if (phone.getNumber()<1000 || phone.getNumber()>9999) {
			return false;
		}
		
		return true;
	}

		
	}
	
	



