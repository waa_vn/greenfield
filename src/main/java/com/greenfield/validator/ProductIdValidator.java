package com.greenfield.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.greenfield.domain.Product;
import com.greenfield.exception.ProductNotFoundException;
import com.greenfield.services.ProductService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


public class ProductIdValidator implements ConstraintValidator<ProductId, Product>{
	
	@Autowired
	private ProductService productService;

	public void initialize(ProductId constraintAnnotation) {
		//  intentionally left blank; this is the place to initialize the constraint annotation for any sensible default values.
	}

	
	public boolean isValid(Product value, ConstraintValidatorContext context) {		
		// Update case:
		System.out.println("Product valid"+ value);
		if (value!=null && value.getId()!=null) {

			return true;
		}
		// Insert case:
		try {
			if (StringUtils.isNotBlank(value.getProductId())) {
				Product product = productService.getProductById(value.getProductId());
				System.out.println("Product valid exist."+ product);
				if(product!= null) {
					context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
              			.addPropertyNode("productId")
              			.addConstraintViolation();
					return false;
				}
			}
		} catch (ProductNotFoundException e) {
			return true;
		} 
		return true;
	}
	
}
