package com.greenfield.formatter;

import java.util.Locale;

import org.springframework.expression.ParseException;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import com.greenfield.domain.Phone;

@Component
public class PhoneFormatter implements Formatter<Phone> {

	@Override
	public String print(Phone phone, Locale locale) {
		return phone.getArea() + "-" + phone.getPrefix() + "-" + phone.getNumber();
	}

	@Override
	public Phone parse(String source, Locale locale) throws ParseException {
		Phone phone = null;
		try {
			// System.out.println("parse: " + source);
			int area = Integer.parseInt(source.substring(0, 2));
			int prefix = Integer.parseInt(source.substring(4, 6));
			int number = Integer.parseInt(source.substring(8, 11));
			phone = new Phone(area, prefix, number);
		} catch (Exception e) {
			System.out.println("Error - Bad Phone Format");
		}
		return phone;
	}
}