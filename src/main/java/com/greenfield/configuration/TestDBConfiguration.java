package com.greenfield.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Profile("test")
@Configuration
@PropertySource({"classpath:application.properties", "classpath:application-test.properties"})
public class TestDBConfiguration extends DBConfiguration { }