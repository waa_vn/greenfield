package com.greenfield.configuration;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.greenfield.api.domain.ApiError;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

  final static String[] URL_MATCHERS = {"/login/**", "/register"};

  @Bean
	public HandlerMappingIntrospector mvcHandlerMappingIntrospector() {
		return new HandlerMappingIntrospector();
  }
  
  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Order(1)
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  public static class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    Environment environment;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth
        .jdbcAuthentication()
        .dataSource(dataSource)
        .passwordEncoder(passwordEncoder)
        .usersByUsernameQuery(environment.getProperty("query.login.usersByUsernameQuery"))
        .authoritiesByUsernameQuery(environment.getProperty("query.login.authoritiesByUsernameQuery"));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http
        .antMatcher("/api/**")
        .cors()
        .and()
        .csrf().disable()
        .anonymous().disable()
        .authorizeRequests()
          .anyRequest().hasAnyRole("ADMIN", "USER")
        .and()
        .exceptionHandling()
          .accessDeniedHandler(accessDeniedHandler())
          .authenticationEntryPoint(authenticationEntryPoint());
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
      return new AccessDeniedHandler() {
        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response,
            AccessDeniedException accessDeniedException) throws IOException, ServletException {
          ApiError error = new ApiError(HttpStatus.FORBIDDEN, "Access denied", new ArrayList<>());
          OutputStream out = response.getOutputStream();
          ObjectMapper mapper = new ObjectMapper();
          mapper.writeValue(out, error);
          out.flush();
        }
      };
    }

    @Bean
    AuthenticationEntryPoint authenticationEntryPoint() {
      return new AuthenticationEntryPoint() {
        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) throws IOException, ServletException {
          ApiError error = new ApiError(HttpStatus.UNAUTHORIZED, "Unauthorized", new ArrayList<>());
          OutputStream out = response.getOutputStream();
          ObjectMapper mapper = new ObjectMapper();
          mapper.writeValue(out, error);
          out.flush();
        }
      };
    }
  }

  @Order(2)
  @Configuration
  public static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    Environment environment;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth
        .jdbcAuthentication()
        .dataSource(dataSource)
        .passwordEncoder(passwordEncoder)
        .usersByUsernameQuery(environment.getProperty("query.login.usersByUsernameQuery"))
        .authoritiesByUsernameQuery(environment.getProperty("query.login.authoritiesByUsernameQuery"));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http
        .antMatcher("/**")
        .csrf()
        .and()
        .authorizeRequests()
          .antMatchers("/admin/**").hasRole("ADMIN")
          .antMatchers(URL_MATCHERS).permitAll()
          .anyRequest().hasAnyRole("ADMIN", "USER")
        // .and()
        // .httpBasic()
        .and()
        .formLogin()
          .loginPage("/login")
          .defaultSuccessUrl("/")
          .failureUrl("/loginfailed")
          .permitAll()
        .and()
        .headers()
        .frameOptions().sameOrigin()
        .httpStrictTransportSecurity().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
      web
        .ignoring()
        .antMatchers(
          HttpMethod.GET,
          "/resources/**",
          "/*.html",
          "/**/favicon.ico",
          "/**/*.html",
          "/**/*.css",
          "/**/*.js",
          "/**/*.png",
          "/fonts/**"
        );
    }
  }
}