package com.greenfield;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import com.greenfield.configuration.ApiConfiguration;
import com.greenfield.configuration.ChatConfiguration;
import com.greenfield.configuration.PersistenceConfiguration;
import com.greenfield.configuration.ChatSecurityConfiguration;
import com.greenfield.configuration.DBConfiguration;
import com.greenfield.configuration.SecurityConfiguration;
import com.greenfield.configuration.ServiceConfiguration;
import com.greenfield.configuration.TestDBConfiguration;
import com.greenfield.configuration.WebConfiguration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import static org.springframework.security.config.BeanIds.SPRING_SECURITY_FILTER_CHAIN;

import java.util.EnumSet;
public class GreenFieldWebApplication implements WebApplicationInitializer {

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    // root Application Context
    AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
    rootContext.register(
      DBConfiguration.class,
      TestDBConfiguration.class,
      PersistenceConfiguration.class,
      ServiceConfiguration.class,
      SecurityConfiguration.class
    );
    servletContext.addListener(new ContextLoaderListener(rootContext));

    // Api application context
    AnnotationConfigWebApplicationContext apiContext = new AnnotationConfigWebApplicationContext();
    apiContext.setParent(rootContext);
    apiContext.register(ApiConfiguration.class);
    DispatcherServlet api = new DispatcherServlet(apiContext);
    api.setThrowExceptionIfNoHandlerFound(true);
    ServletRegistration.Dynamic apiDispatcher = servletContext.addServlet("apiDispatcher", api);
    apiDispatcher.setLoadOnStartup(1);
    apiDispatcher.addMapping("/api/*");
    
    // Web application context
    AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();
    webContext.setParent(rootContext);
    webContext.register(
      WebConfiguration.class,
      ChatConfiguration.class,
      ChatSecurityConfiguration.class
    );
    DispatcherServlet web = new DispatcherServlet(webContext);
    web.setThrowExceptionIfNoHandlerFound(true);
    ServletRegistration.Dynamic webDispatcher = servletContext.addServlet("webDispatcher", web);
    webDispatcher.setLoadOnStartup(1);
    webDispatcher.addMapping("/");

    //security
    servletContext
      .addFilter(SPRING_SECURITY_FILTER_CHAIN, new DelegatingFilterProxy("springSecurityFilterChain"))
      .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "/*");


    // Localization filter
    // EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ERROR);
  
    // CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
    //   characterEncodingFilter.setEncoding("UTF-8");
    //   characterEncodingFilter.setForceEncoding(true);
    
    // FilterRegistration.Dynamic characterEncoding = servletContext.addFilter("characterEncoding", characterEncodingFilter);
    //   characterEncoding.addMappingForUrlPatterns(dispatcherTypes, true, "/*");
    //   characterEncoding.setAsyncSupported(true);
  }
}