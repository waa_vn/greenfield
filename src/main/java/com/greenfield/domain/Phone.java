/**
 * 
 */
package com.greenfield.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

@Entity
public class Phone implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@Range(min = 100, max = 999, message = "{Phone.validation}")
 	private Integer area;
	
	@NotNull
	@Range(min = 100, max = 999, message = "{Phone.validation}")
	private Integer prefix;
 	
	@NotNull
	@Range(min = 1000, max = 9999, message = "{Phone.validation}")
	private Integer number;
	
	public Phone() {
		
	}
	
	public Phone(Integer area, Integer prefix, Integer number) {
		this.area = area;
		this.prefix = prefix;
		this.number = number;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

 	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getPrefix() {
		return prefix;
	}

	public void setPrefix(Integer prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return "Phone [area=" + area + ", prefix=" + prefix + ", number=" + number + "]";
	}
	
	
	
}
