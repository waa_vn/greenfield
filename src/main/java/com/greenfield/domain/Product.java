package com.greenfield.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.GenericGenerator;

import com.greenfield.validator.ProductId;

//import com.greenfield.domain.generator.StringPrefixedSequenceIdGenerator;
//import com.greenfield.validator.ProductId;

//import org.hibernate.annotations.Parameter;
import org.springframework.web.multipart.MultipartFile;

@Entity
@ProductId
public class Product implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -5007081803976008934L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private Long id;

	@NotEmpty
	@Pattern(regexp="P-[0-9]{5}", message="{Pattern.Product.productId.validation}")
	@Column(unique = true)
	private String productId;
	
	@Size(min=4, max=150, message="{Size.Product.name.validation}")
	private String name;
	
	@Min(value=0) //, message="Min.Product.unitPrice.validation}")
	@Digits(integer=8, fraction=2, message="{Digits.Product.unitPrice.validation}")
	@NotNull
	private BigDecimal unitPrice;
	@Column(length = 2000)
	private String description;
	private String manufacturer;

	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;

	private long unitsInStock;
	private long unitsInOrder;
	private boolean discontinued;
	private String inCondition;

	private String imageUrl;
	@Transient
	private MultipartFile  productImage;

	public String getProductId() {
		
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public long getUnitsInStock() {
		return unitsInStock;
	}

	public void setUnitsInStock(long unitsInStock) {
		this.unitsInStock = unitsInStock;
	}

	public long getUnitsInOrder() {
		return unitsInOrder;
	}

	public void setUnitsInOrder(long unitsInOrder) {
		this.unitsInOrder = unitsInOrder;
	}

	public boolean isDiscontinued() {
		return discontinued;
	}

	public void setDiscontinued(boolean discontinued) {
		this.discontinued = discontinued;
	}

	public String getInCondition() {
		return inCondition;
	}

	public void setInCondition(String inCondition) {
		this.inCondition = inCondition;
	}


	public MultipartFile getProductImage() {
		return productImage;
	}

	public void setProductImage(MultipartFile productImage) {
		this.productImage = productImage;
	}

	@Override
	public String toString() {
		
		return "Product:"+ this.name + ", productId:"+ this.productId + ", id:"+ this.id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
}