package com.greenfield.domain;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class User implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5030700441146490998L;

    @Id
    @GeneratedValue
    @Column
    private long id;

    @NotEmpty (message = "{NotEmpty}")
    @Column
    private String username;

    @NotEmpty (message = "{NotEmpty}")
    @Column
    private String password;

   @Transient
    private String verifyPassword;

    @NotNull
    @Column
    private short enabled;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Authority authority;

    public short getEnabled() {
        return enabled;
    }

    public void setEnabled(short enabled) {
        this.enabled = enabled;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Authority getAuthority() {
        return authority;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    public String getVerifyPassword() {
        return verifyPassword;
    }

    public void setVerifyPassword(String verifyPassword) {
        this.verifyPassword = verifyPassword;
    }
}
