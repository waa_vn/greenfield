package com.greenfield.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.greenfield.validator.PhoneAnotation;

@Entity
public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotEmpty
	private String customerId;

	@NotEmpty
	private String name;
	@Email(message = "{Email.validation}")
	private String email;
	
	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	private Address billingAddress;
	@Valid
	//@PhoneAnotation
	//@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	private Phone phoneNumber;
	
	public Customer() {
		this.billingAddress = new Address();
		this.phoneNumber = new Phone();
	}
	
	public Customer(String customerId, String name, String email, Address billingAddress, Phone phoneNumber) {
		this.customerId = customerId;
		this.name = name;
		this.email = email;
		this.billingAddress = billingAddress;
		this.phoneNumber = phoneNumber;
	}



	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	
	public Phone getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Phone phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	

	public void setEmail(String email) {
		this.email = email;
	}

	
}
