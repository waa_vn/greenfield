package com.greenfield.services;

import com.greenfield.domain.Order;

public interface OrderService {
	
	public void saveOrder(Order order);
}
