package com.greenfield.services.impl;

import com.greenfield.domain.User;
import com.greenfield.repositories.UserRepository;
import com.greenfield.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;


    @Override
    public User register(User user) {
       return userRepository.save(user);
    }

    @Override
    public User lookUpUserByUserName(String userName) {
        return userRepository.lookUpUserByUserName(userName);
    }
}
