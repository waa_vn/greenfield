package com.greenfield.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greenfield.domain.Order;
import com.greenfield.repositories.OrderRepository;
import com.greenfield.services.OrderService;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderRepository orderRepository;
	
	public void saveOrder(Order order) {
		//Save order
		orderRepository.save(order);
	}


}
