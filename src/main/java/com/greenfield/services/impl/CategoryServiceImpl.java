package com.greenfield.services.impl;

import java.util.List;

import com.greenfield.domain.Category;
import com.greenfield.repositories.CategoryRepository;
import com.greenfield.services.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional()
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository repos;

    @Override
    public List<Category> getAll() {
        
        return (List<Category>) repos.findAll();

    }

    @Override
    public Category getOneIncludeProducts(Long id) {
        
        return repos.findById(id).orElse(null);

    }
 
    
    public Category getOne(Long id) {
        
        return repos.findById(id).orElse(null);

    }
}