package com.greenfield.services.impl;

import java.util.List;

import com.greenfield.domain.Product;
import com.greenfield.repositories.ProductRepository;
import com.greenfield.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository repos;

	@Override
	public List<Product> getAllProducts() {
		//  return new ArrayList<Product>();
		return (List<Product>) repos.findAll();
	}


	@Override
	public Product getProductById(String id) {
		return repos.getByProductId(id);
	}

	@Override
	public Product getProductById(Long id) {
		return repos.findById(id).orElse(null);
	}

	@Override
	public Product save(Product product) {
		
		return repos.save(product);
	}

}