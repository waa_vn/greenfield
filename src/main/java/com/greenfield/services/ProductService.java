package com.greenfield.services;

import java.util.List;

import com.greenfield.domain.Product;

public interface ProductService {
  List<Product> getAllProducts();

  Product getProductById(Long value);

  Product getProductById(String value);

  Product save(Product product);
}