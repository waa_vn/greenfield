package com.greenfield.services;

import java.util.List;

import com.greenfield.domain.Category;

public interface CategoryService {
    List<Category> getAll();

	Category getOneIncludeProducts(Long id);

    Category getOne(Long id);
}