package com.greenfield.services;

import com.greenfield.domain.User;

public interface UserService {

    enum USER_ROLE {
        ROLE_ADMIN,
        ROLE_USER
    }

    User register(User user);

    User lookUpUserByUserName(String userName);
}
