package com.greenfield.repositories;

import org.springframework.data.repository.CrudRepository;

import com.greenfield.domain.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {
	
}
