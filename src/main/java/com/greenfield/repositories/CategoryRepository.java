package com.greenfield.repositories;

import com.greenfield.domain.Category;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long>{

    @Query(nativeQuery = false, value="FROM Category AS c JOIN c.products AS p WHERE c.id = :id")
    public Category getOneWithProduct(@Param("id") Long id);
    
}
