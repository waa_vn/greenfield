package com.greenfield.repositories;

import com.greenfield.domain.Product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>{ 
    
    @Query("SELECT p FROM Product p WHERE p.productId=:id")
    Product getByProductId(@Param("id") String productId);
}
