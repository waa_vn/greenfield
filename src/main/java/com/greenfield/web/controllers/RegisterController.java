package com.greenfield.web.controllers;

import com.greenfield.domain.Authority;
import com.greenfield.domain.User;
import com.greenfield.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Locale;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {
    @Autowired
    UserService userService;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String register(Model model, User user) {
        model.addAttribute("user", user);
        return "register";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "register";
        }
        if (!user.getPassword().equals(user.getVerifyPassword())) {
            result.addError(new ObjectError("userName",messageSource.getMessage("password.verification", null, Locale.ENGLISH)));
            return "register";
        }

        User existUser = userService.lookUpUserByUserName(user.getUsername());
        if (existUser != null) {
            result.addError(new ObjectError("userName",messageSource.getMessage("username.duplicated", null, Locale.ENGLISH)));
            return "register";
        }

        Authority authority = new Authority();
        authority.setAuthority(UserService.USER_ROLE.ROLE_USER.name());
        authority.setUsername(user.getUsername());
        user.setAuthority(authority);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.register(user);
        return "redirect:/";

    }
}
