package com.greenfield.web.controllers;

import com.greenfield.domain.ChatMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ChatController {
  @Autowired
  private SimpMessageSendingOperations messagingTemplate;

  @RequestMapping("/chat")
  public String chatPage() {
    return "chat";
  }

  // user actions
  @MessageMapping("/ask")
  @SendTo("/secured/questions")
  public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
    return chatMessage;
  }

  @MessageMapping("/user.join")
  @SendTo("/secured/questions")
  public ChatMessage userJoin(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
    headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
    return chatMessage;
  }

  // seller actions
  @MessageMapping("/seller.join")
  @SendTo("/secured/seller/activated")
  public ChatMessage sellerJoin(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
    headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
    return chatMessage;
  }

  @MessageMapping("/seller.reply")
  public void sellerReply(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
    String to = chatMessage.getTo();
    messagingTemplate.convertAndSend("/secured/user/queue/specific-user-" + to, chatMessage);
  }
}