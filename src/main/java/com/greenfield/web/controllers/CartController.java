package com.greenfield.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/cart")
public class CartController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String get(HttpServletRequest request, Model model) {
		model.addAttribute("cartId", request.getSession(true).getId());
		return "redirect:/cart/getCart";
	}
	
	@RequestMapping(value = "/getCart", method = RequestMethod.GET)
	public String getCart(Model model) {
		return "cart";
	}
}
