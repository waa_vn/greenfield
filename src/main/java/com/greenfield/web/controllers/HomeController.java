package com.greenfield.web.controllers;

import java.util.List;

import com.greenfield.domain.Category;
import com.greenfield.services.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
  @Autowired
  CategoryService categoryService;
  @RequestMapping("/")
  public String homepage(Model model) {
    List<Category> categories = categoryService.getAll();
    model.addAttribute("categories", categories);
    return "homepage";
  }
}