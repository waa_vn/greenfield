package com.greenfield.web.controllers;

import com.greenfield.domain.Category;
import com.greenfield.services.CategoryService;
import com.greenfield.web.exceptions.CategoryNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/category")
public class CategoryController {

    // private static final Logger log = new Logger(CategoryController.class);

    @Autowired
    CategoryService service;
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public String requestMethodName(@PathVariable Long id, Model model) {
        Category cat = service.getOneIncludeProducts(id);

        if (cat == null)
            throw new CategoryNotFoundException();
        
        model.addAttribute("category", cat);
        model.addAttribute("products", cat.getProducts());
        return "category";
    }
    
}