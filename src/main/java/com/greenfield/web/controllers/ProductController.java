package com.greenfield.web.controllers;

import com.greenfield.domain.Product;
import com.greenfield.services.CategoryService;
import com.greenfield.services.ProductService;
import com.greenfield.web.exceptions.ProductNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller

public class ProductController {

    @Autowired
    ProductService service;

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value="/product", method=RequestMethod.GET)
    public String detail(@RequestParam String id, Model model) {
       
        Product prod = service.getProductById(id);
        if (prod == null)
            throw new ProductNotFoundException();

        model.addAttribute("product", prod);

        return "product";
    }
    
    @RequestMapping(value="/admin/product", method=RequestMethod.GET)
    public String manage(Model model) {
        
        model.addAttribute("categories", categoryService.getAll());

        return "/admin/product";
    }



}