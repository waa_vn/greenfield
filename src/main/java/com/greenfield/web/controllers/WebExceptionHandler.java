package com.greenfield.web.controllers;

import java.sql.SQLException;

import com.greenfield.web.exceptions.CategoryNotFoundException;
import com.greenfield.web.exceptions.ProductNotFoundException;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class WebExceptionHandler {
  @ExceptionHandler({ Exception.class, SQLException.class, DataAccessException.class })
	public String handleAllExceptions(Exception exception, Model model) {
  		model.addAttribute("exception", exception);
		return "error";
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public String forbidden(Exception exception) {
		return "forbidden";
	}

	@ExceptionHandler({NoHandlerFoundException.class, ProductNotFoundException.class, CategoryNotFoundException.class})
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String notfound(Exception exception) {
		return "notFound";
	}
}