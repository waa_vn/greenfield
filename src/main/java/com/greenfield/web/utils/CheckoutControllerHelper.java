package com.greenfield.web.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.greenfield.domain.Cart;
import com.greenfield.domain.Order;
import com.greenfield.services.CartService;
import com.greenfield.services.OrderService;

@Component
public class CheckoutControllerHelper {
  @Autowired
  OrderService orderService;
  @Autowired
  CartService cartService;

  public Long saveOrder(Order order) {
	  order.setOrderId(Math.abs(new Random().nextLong()));
	  order.setOrderedDate(new Date());
	  order.setStatus("New");
	  
	  //save order
	  orderService.saveOrder(order);
	  return order.getOrderId();
  }

  public Cart validateCart(String cartId) {
    return cartService.validate(cartId);
  }
  
}