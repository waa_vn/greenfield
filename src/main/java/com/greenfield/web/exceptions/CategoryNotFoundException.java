package com.greenfield.web.exceptions;

public class CategoryNotFoundException extends RuntimeException {
    /**
    *
    */
    private static final long serialVersionUID = -3942532203545497042L;

    public CategoryNotFoundException() {
        super();
      }
    
      public CategoryNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
      }
    
      public CategoryNotFoundException(final String message) {
        super(message);
      }
    
      public CategoryNotFoundException(final Throwable cause) {
        super(cause);
      }
    
}