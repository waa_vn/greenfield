package com.greenfield.api.controllers;

import java.util.List;

import com.greenfield.domain.Category;
import com.greenfield.services.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/categories")
public class CategoryController {
  @Autowired
  CategoryService categoryService;

  @RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody List<Category> getAllProducts() {
		return categoryService.getAll();
	}
}