package com.greenfield.api.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.greenfield.api.exceptions.ImageUploadException;
import com.greenfield.api.exceptions.ResourceNotFoundException;
import com.greenfield.domain.Category;
import com.greenfield.domain.Product;
import com.greenfield.services.CategoryService;
import com.greenfield.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/products")
public class ProductController {
	@Autowired
	ProductService productService;
	@Autowired
	CategoryService categoryService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody List<Product> getAllProducts() {
		return productService.getAllProducts();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Product getProduct(@PathVariable("id") Long id) {
		Product prod = productService.getProductById(id);
		if (prod != null)
			return prod;

		throw new ResourceNotFoundException("Product id:" + id + " does not exist.");
	}

	@RequestMapping(value="/save", method=RequestMethod.POST)
	@ResponseBody
	public Product addProduct(@RequestBody @Valid Product product) {
		System.out.println("saving ....  " + product);
		System.out.println("prod cat.... " + product.getCategory());
		// if ( product.getCategory() != null) {
		// 	Long catId = product.getCategory().getId();
		// 	Category cat = categoryService.getOne(catId);
		// 	product.setCategory(cat);
		// }
		return productService.save(product);
	}

}