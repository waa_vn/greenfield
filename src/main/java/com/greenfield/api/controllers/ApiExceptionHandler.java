package com.greenfield.api.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.AuthenticationException;

import com.greenfield.api.domain.ApiError;
import com.greenfield.api.exceptions.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ Exception.class, SQLException.class, DataAccessException.class })
	public final ResponseEntity<ApiError> handleAllExceptions(Exception ex, WebRequest request) {
		ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error",
				new ApiError.Error("server.error", ex.getLocalizedMessage()));
		return new ResponseEntity<ApiError>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ ResourceNotFoundException.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public final ResponseEntity<ApiError> handleResourceNotFoundException(Exception ex, WebRequest request) {
		ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Record Not Found",
				new ApiError.Error("record.notfound", ex.getLocalizedMessage()));
		return new ResponseEntity<ApiError>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ AccessDeniedException.class })
	public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
		ApiError error = new ApiError(HttpStatus.FORBIDDEN, "Access denied");
		return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler({ AuthenticationException.class })
	public ResponseEntity<Object> handleAuthenticationException(Exception ex, WebRequest request) {
		ApiError error = new ApiError(HttpStatus.UNAUTHORIZED, "Unauthorized");
		return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
	}

	@Autowired
	MessageSourceAccessor messageAccessor;

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
		HttpHeaders headers, HttpStatus status, WebRequest request) {

		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Validation Failed");

		List<ApiError.Error> details = new ArrayList<>();

		if (ex.getBindingResult().hasFieldErrors()){
			for (FieldError error : ex.getBindingResult().getFieldErrors()) {
				details.add(new ApiError.Error(error.getField(), messageAccessor.getMessage(error) ) );
			}
			apiError.setType("ValidationError");
		}else{
			for (ObjectError error : ex.getBindingResult().getAllErrors()) {
				details.add(new ApiError.Error(error.getObjectName(), messageAccessor.getMessage(error) ) );
			}
		}
			
		apiError.setErrors(details);

		return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
	}
}