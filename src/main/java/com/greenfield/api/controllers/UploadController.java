package com.greenfield.api.controllers;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.greenfield.api.domain.ApiError;
import com.greenfield.api.domain.UploadResult;
import com.greenfield.api.exceptions.ImageUploadException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriUtils;

@Controller
public class UploadController {

    private final Logger logger = LoggerFactory.getLogger(UploadController.class);


    // 3.1.1 Single file upload
    @RequestMapping(value="/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> uploadFile(@RequestParam("productImage") MultipartFile uploadfile, HttpServletRequest request) {

        logger.debug("Single file upload!");

        String returnPath;

        if (uploadfile.isEmpty()) {
            return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "please select a file!"), HttpStatus.BAD_REQUEST);
        }

        try {
            String rootDirectory = request.getServletContext().getRealPath("/");
            returnPath = saveUploadedFiles(uploadfile, rootDirectory);

        } catch (IOException e) {
            return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Error: " + e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity<>(new UploadResult(returnPath), new HttpHeaders(), HttpStatus.OK);

    }

    private String saveUploadedFiles(MultipartFile file, String basePath) throws IOException {

        

        if (file.isEmpty()) {
            return "Empty file";
        }
        // String fullPath = basePath + file.getOriginalFilename();
        String savePath = "resources" + File.separator + "upLoadImages" + File.separator + file.getOriginalFilename();
        String returnPath = "resources" + File.separator + "upLoadImages" + File.separator 
                            + UriUtils.encode(UriUtils.decode(file.getOriginalFilename(), "UTF-8"), "UTF-8");
        // byte[] bytes = file.getBytes();
        // Path path = Paths.get(fullPath);
        // Files.write(path, bytes);

        try {
            file.transferTo(new File(basePath+savePath));
            System.out.println("FFF"+ basePath+savePath);
        } catch (Exception e) {
            throw new ImageUploadException("Product Image saving failed", e);
        }

        return returnPath;
        

    }

    @ExceptionHandler(ImageUploadException.class)
	public String handleError(HttpServletRequest req, ImageUploadException ex) {
		req.setAttribute("exception", ex);
		req.setAttribute("javax.servlet.error.message", ex.getMessage());
		
		return "error";
	}
}