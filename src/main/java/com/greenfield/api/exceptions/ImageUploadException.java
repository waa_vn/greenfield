package com.greenfield.api.exceptions;

public class ImageUploadException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6195253655906005293L;
	public ImageUploadException(String message, Throwable e) {
		super(message, e);
	}
}
