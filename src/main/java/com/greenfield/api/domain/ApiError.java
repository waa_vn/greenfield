package com.greenfield.api.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ApiError {
  private HttpStatus status;
  private String message;
  private String type;
  private List<Error> errors;

  public ApiError() {
    super();
  }

  public ApiError(final HttpStatus status, final String message, final List<Error> errors) {
    super();
    this.status = status;
    this.message = message;
    this.errors = errors;
  }

  public ApiError(final HttpStatus status, final String message, final Error error) {
    super();
    this.status = status;
    this.message = message;
    errors = Arrays.asList(error);
  }

  public ApiError(final HttpStatus status, final String message) {
    super();
    this.status = status;
    this.message = message;
    errors = new ArrayList<>();
  }

  public HttpStatus getStatus() {
    return status;
  }

  public void setStatus(final HttpStatus status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public List<Error> getErrors() {
    return errors;
  }

  public void setErrors(final List<Error> errors) {
    this.errors = errors;
  }

  public void setError(final Error error) {
    errors = Arrays.asList(error);
  }

  public static class Error {
    private String fieldName;
    private String message;

    public Error(String fieldName, String message){
      this.fieldName = fieldName;
      this.message = message;
    }

    public String getFieldName() {
      return fieldName;
    }

    public void setFieldName(String fieldName) {
      this.fieldName = fieldName;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}