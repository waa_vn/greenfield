package com.greenfield.api.domain;

public class UploadResult {
    String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public UploadResult(String filePath) {
        this.filePath = filePath;
    }
}