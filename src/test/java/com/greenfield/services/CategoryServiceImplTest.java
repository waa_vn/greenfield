package com.greenfield.services;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

import com.greenfield.builder.CategoryBuilder;
import com.greenfield.builder.ProductBuilder;
import com.greenfield.domain.Category;
import com.greenfield.repositories.CategoryRepository;
import com.greenfield.services.impl.CategoryServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CategoryServiceImplTest {
  @Mock
  CategoryRepository categoryRepository;
  @InjectMocks
  private CategoryServiceImpl categoryService;

  List<Category> categoryList = new ArrayList<Category>();

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    categoryList.add(
      new CategoryBuilder()
        .withId(1L)
        .withName("Sports")
        .withProducts(Arrays.asList(
          new ProductBuilder()
            .withId(1L)
            .withDescription("Two wheels")
            .withName("Bicycle")
            .build()
        ))
        .build()
    );
    categoryList.add(
      new CategoryBuilder()
        .withId(1L)
        .withName("Solids")
        .withProducts(Arrays.asList(
          new ProductBuilder()
            .withId(2L)
            .withDescription("6 sides")
            .withName("Cube")
            .build()
        ))
        .build()
    );
  }

  @Test
  public void getAll() {
    // Arrange
    when(categoryRepository.findAll()).thenReturn(categoryList);

    // Act
    List<Category> categories = categoryService.getAll();

    // Assert
    assertThat(categories, hasItem(
      allOf(
        hasProperty("id", is(1L)),
        hasProperty("name", is("Sports")),
        hasProperty("products", hasItem(
          allOf(
            hasProperty("id", is(1L)),
            hasProperty("description", is("Two wheels")),
            hasProperty("name", is("Bicycle"))
          )
        ))
      )
    ));
  }

  @Test
  public void getOneIncludeProducts() {
    // Arrange
    when(categoryRepository.findById(1L)).thenReturn(Optional.of(categoryList.get(0)));

    // Act
    Category category = categoryService.getOneIncludeProducts(1L);

    // Assert
    assertThat(category, allOf(
      hasProperty("id", is(1L)),
      hasProperty("name", is("Sports"))
    ));
  }
}