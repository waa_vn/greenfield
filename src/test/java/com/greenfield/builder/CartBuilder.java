package com.greenfield.builder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.greenfield.domain.Cart;
import com.greenfield.domain.Category;
import com.greenfield.domain.LineItem;
import com.greenfield.domain.Product;



public class CartBuilder {

	public final static String cartID = "12345ABCDEF";
	
	private Cart cart;
	
	public CartBuilder() {
		cart = new Cart();
		cart.setCartId(cartID);
		Map<String, LineItem> cartItems = new HashMap<String,LineItem>();
		
		Category category = new Category();
		category.setName("Tablet");
		Product product = new Product();
		product.setCategory(category);
		product.setName("Google Nexus");
		product.setDescription("Google Nexus");
		product.setManufacturer("Google");
		product.setProductId("P1236");
		product.setUnitPrice(new BigDecimal(300));
		product.setUnitsInStock(1000);
		LineItem lineItem = new LineItem(product);
		cartItems.put("P1236", lineItem);
		
		product = new Product();
		category = new Category();
		category.setName("Smart Phone");
		product.setCategory(category);
		product.setName("Apple Iphone 5x");
		product.setDescription("Apple Iphone 5x");
		product.setManufacturer("Apple");
		product.setProductId("P1234");
		product.setUnitPrice(new BigDecimal(200));
		product.setUnitsInStock(1000);
		lineItem = new LineItem(product);
		cartItems.put("P1234", lineItem);
		
		product = new Product();
		category = new Category();
		category.setName("Laptop");
		product.setCategory(category);
		product.setName("Dell Inspiron");
		product.setDescription("Dell Inspiron");
		product.setManufacturer("Dell");
		product.setProductId("P1235");
		product.setUnitPrice(new BigDecimal(100));
		product.setUnitsInStock(1000);
		lineItem = new LineItem(product);
		cartItems.put("P1235", lineItem);
		
		cart.setCartItems(cartItems);
		cart.updateGrandTotal();
		
		
	}
	
	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}



}
