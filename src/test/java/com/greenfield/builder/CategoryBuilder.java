package com.greenfield.builder;

import java.util.List;

import com.greenfield.domain.Category;
import com.greenfield.domain.Product;

public class CategoryBuilder {

  private Category category;

  public CategoryBuilder() {
    this.category = new Category();
  }

  public CategoryBuilder withName(String name) {
    this.category.setName(name);
    return this;
  }

  public CategoryBuilder withId(Long id) {
    this.category.setId(id);
    return this;
  }

  public CategoryBuilder withProducts(List<Product> products) {
    this.category.setProducts(products);
    return this;
  }

  public Category build() {
    return category;
  }

}
