package com.greenfield.utils;

import com.greenfield.configuration.WebConfiguration;
import com.greenfield.services.CartService;
import com.greenfield.services.CategoryService;
import com.greenfield.services.OrderService;
import com.greenfield.services.ProductService;
import com.greenfield.services.UserService;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class WebMockConfiguration extends WebConfiguration {
  @Bean
  public CategoryService categoryServiceMock() {
    return Mockito.mock(CategoryService.class);
  }

  @Bean
  public ProductService productServiceMock() {
    return Mockito.mock(ProductService.class);
  }

  @Bean
  public CartService cartServiceMock() {
    return Mockito.mock(CartService.class);
  }

  @Bean
  public UserService userServiceMock() {
    return Mockito.mock(UserService.class);
  }

  @Bean
  public BCryptPasswordEncoder bcryptPasswordEncoderMock() {
    return Mockito.mock(BCryptPasswordEncoder.class);
  }

  @Bean
  public OrderService orderServiceMock() {
    return Mockito.mock(OrderService.class);
  }

  @Bean
  public SimpMessageSendingOperations messagingTemplateMock() {
    return Mockito.mock(SimpMessageSendingOperations.class);
  }
}