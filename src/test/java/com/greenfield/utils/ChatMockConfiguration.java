package com.greenfield.utils;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

@Configuration
public class ChatMockConfiguration {
  @Bean
  public SimpMessageSendingOperations messagingTemplateMock() {
    return Mockito.mock(SimpMessageSendingOperations.class);
  }
}