package com.greenfield.utils;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityMockConfiguration {
  @Bean
  public BCryptPasswordEncoder bcryptPasswordEncoderMock() {
    return Mockito.mock(BCryptPasswordEncoder.class);
  }
}