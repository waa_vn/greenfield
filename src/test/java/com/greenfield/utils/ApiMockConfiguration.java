package com.greenfield.utils;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.greenfield.configuration.ApiConfiguration;
import com.greenfield.services.CartService;
import com.greenfield.services.CategoryService;
import com.greenfield.services.ProductService;

@Configuration
public class ApiMockConfiguration extends ApiConfiguration {
  
  @Bean
  public CartService cartServiceMock() {
    return Mockito.mock(CartService.class);
  }

  @Bean
  public ProductService productServiceMock() {
    return Mockito.mock(ProductService.class);
  }

  @Bean
  public CategoryService categoryServiceMock() {
    return Mockito.mock(CategoryService.class);
  }
}