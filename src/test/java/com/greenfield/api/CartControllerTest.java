package com.greenfield.api;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.greenfield.builder.CartBuilder;
import com.greenfield.services.CartService;
import com.greenfield.utils.ApiMockConfiguration;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApiMockConfiguration.class })
@WebAppConfiguration
@ActiveProfiles("test")
public class CartControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private CartService cartServiceMock;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private CartBuilder cartBuilder = null;
	
	@Before
	public void setup() {
		this.cartBuilder = new CartBuilder();
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	/**
	 * Unit Test (positive case) to get Cart by CartId
	 * @throws Exception display error message in log
	 */
	@Test
	public void getCartWhenValidRequest() throws Exception {
		Mockito.reset(cartServiceMock);
		when(cartServiceMock.read(CartBuilder.cartID)).thenReturn(cartBuilder.getCart());
		try {
			String requestUrl = "/cart/" + CartBuilder.cartID;
			mockMvc.perform(get(requestUrl).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.cartId").value(CartBuilder.cartID))
				.andExpect(jsonPath("$.grandTotal").value(600));
		} catch (AssertionError e) {
			System.out.println("Error Message: " + e.getMessage());
			throw e;
		}
		
		//verify(cartServiceMock, times(2)).read(CartBuilder.cartID);
        //verifyNoMoreInteractions(cartServiceMock);
	}
	
	/**
	 * Unit Test (negative case) to get Cart by empty CartId
	 * @throws Exception display error message in log
	 */
	 @Test
	 public void getCartWhenInValidRequest() throws Exception {
	 	when(cartServiceMock.read(CartBuilder.cartID)).thenReturn(cartBuilder.getCart());
	 	try {
	 		String requestUrl = "/cart/" + "";
	 		mockMvc.perform(get(requestUrl).accept(MediaType.APPLICATION_JSON))
	 		.andExpect(status().isMethodNotAllowed()); //405 error
	 	} catch (AssertionError e) {
	 		System.out.println("Error Message: " + e.getMessage());
	 		throw e;
	 	}
	 	
	 }

	 /**
	 * Unit Test (positive case) to add to Cart with valid productId
	 * @throws Exception display error message in log
	 */
	 @Test
	 public void addProductToCartWhenValidRequest() throws Exception {
	 	try {
	 		String requestUrl = "/cart/add/" + "P-12345";
	 		mockMvc.perform(put(requestUrl).accept(MediaType.APPLICATION_JSON))
	 		.andExpect(status().isInternalServerError()) //500 error because not login
	 		.andExpect(jsonPath("$").doesNotExist());
	 	} catch (AssertionError e) {
	 		System.out.println("Error Message: " + e.getMessage());
	 		throw e;
	 	}
	 }

	 /**
	 * Unit Test (negative case) to add to Cart with invalid productId
	 * @throws Exception display error message in log
	 */
	 @Test
	 public void addProductToCartWhenInValidRequest() throws Exception {
	 	try {
	 		String requestUrl = "/cart/add/" + "zaq12@1"; // invalid ProductId
	 		mockMvc.perform(put(requestUrl))
	 		.andExpect(status().isInternalServerError()); //404 error
	 	} catch (AssertionError e) {
	 		System.out.println("Error Message: " + e.getMessage());
	 		throw e;
	 	}
	 }

}