package com.greenfield.web;

import com.greenfield.utils.WebMockConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = WebMockConfiguration.class)
@ActiveProfiles("test")
public class RegisterControllerTest {
  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  public void shouldRedirectToHome() throws Exception {
    try {
      mockMvc
        .perform(
          post("/register")
            .param("username", "test1")
            .param("password", "test1")
            .param("verifyPassword", "test1")
          )
          .andExpect(status().is3xxRedirection())
          .andExpect(view().name("redirect:/")
        );
    } catch (Exception e) {
      System.out.println("Error Message: " + e.getMessage());
      throw e;
    }
  }
}