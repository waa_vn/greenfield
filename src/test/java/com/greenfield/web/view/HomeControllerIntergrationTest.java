package com.greenfield.web.view;

import com.greenfield.configuration.DBConfiguration;
import com.greenfield.configuration.PersistenceConfiguration;
import com.greenfield.configuration.ServiceConfiguration;
import com.greenfield.configuration.TestDBConfiguration;
import com.greenfield.configuration.WebConfiguration;
import com.greenfield.utils.ChatMockConfiguration;
import com.greenfield.utils.SecurityMockConfiguration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
  classes = {
    MockServletContext.class,
    DBConfiguration.class,
    TestDBConfiguration.class,
    PersistenceConfiguration.class,
    ServiceConfiguration.class,
    SecurityMockConfiguration.class,
    ChatMockConfiguration.class,
    WebConfiguration.class
  }
)
@WebAppConfiguration
@ActiveProfiles("test")
public class HomeControllerIntergrationTest {
  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  public void shouldReturnViewAndModelOfHomePage() throws Exception {
    try {
      mockMvc
        .perform(get("/"))
        .andExpect(status().isOk())
        .andExpect(view().name("homepage"))
        .andExpect(model().attribute("categories", hasSize(1)))
        .andExpect(
          model().attribute("categories", hasItem(
            allOf(
              hasProperty("id", is(1L)),
              hasProperty("name", is("Faux Plants")),
              hasProperty("products",
                hasItem(allOf(
                  hasProperty("id", is(1L)),
                  hasProperty("name", is("name pro")),
                  hasProperty("description", is("des"))
                ))
              )
            )
          ))
        );
    } catch (Exception e) {
      System.out.println("Error Message: " + e.getMessage());
      throw e;
    }
  }
}