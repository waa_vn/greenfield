package com.greenfield.web;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;

import com.greenfield.builder.CategoryBuilder;
import com.greenfield.builder.ProductBuilder;
import com.greenfield.domain.Category;
import com.greenfield.services.CategoryService;
import com.greenfield.utils.WebMockConfiguration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebMockConfiguration.class })
@WebAppConfiguration
@ActiveProfiles("test")
public class HomeControllerTest {
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;
  @Autowired
  private CategoryService categoryServiceMock;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  public void getHomePage_RenderHomeView() throws Exception {
    // Arange
    Category cat1 = new CategoryBuilder()
      .withId(1L)
      .withName("Sports")
      .withProducts(Arrays.asList(
        new ProductBuilder().withId(1L).withName("Bicycle").withDescription("Two wheels").build(), 
        new ProductBuilder().withId(2L).withName("Cube").withDescription("6 sides").build()
      ))
      .build();

    Category cat2 = new CategoryBuilder()
      .withId(2L)
      .withName("Solids")
      .withProducts(Arrays.asList(
        new ProductBuilder().withId(3L).withName("Bicycle").withDescription("Two wheels").build(), 
        new ProductBuilder().withId(4L).withName("Cube").withDescription("6 sides").build()
      ))
      .build();

    Mockito.reset(categoryServiceMock);
    when(categoryServiceMock.getAll()).thenReturn(Arrays.asList(cat1, cat2));

    try {
      // Act
      mockMvc.perform(get("/"))
      // Assert
        .andExpect(status().isOk())
        .andExpect(view().name("homepage"))
        .andExpect(model().attribute("categories", hasSize(2)))
        .andExpect(
          model().attribute("categories", hasItem(
            allOf(
              hasProperty("id", is(1L)),
              hasProperty("name", is("Solids")),
              hasProperty("products",
                hasItem(allOf(
                  hasProperty("id", is(1L)),
                  hasProperty("name", is("Bicycle")),
                  hasProperty("description", is("Two wheels"))
                ))
              )
            )
          ))
        );
    } catch (AssertionError e) {
      System.out.println(e.getMessage());
    }

    // Assert
    verify(categoryServiceMock, times(1)).getAll();
    verifyNoMoreInteractions(categoryServiceMock);
  }
}